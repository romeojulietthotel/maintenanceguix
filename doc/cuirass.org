Cuirass manual is available [[https://guix.gnu.org/cuirass/][here]]. Here are a few notes relative to its
integration.

* Berlin

Cuirass is running on the Berlin build farm. It is configured to use the
[[https://guix.gnu.org/cuirass/manual/cuirass.html#With-the-remote-build-mechanism_002e][remote build]] mode. Around 30 machines, accessible on Berlin local network are
running Cuirass workers. Those machines are named hydra-guix-101 to
hydra-guix-129. Their architecture is x86_64-linux, but they are also
configured to emulate builds on other architectures.

Other machines such as the Overdrives are not physically located on the Berlin
build farm but, are still accessible via [[https://guix.gnu.org/manual/devel/en/html_node/VPN-Services.html][Wireguard]] on the ~10.0.0.0/24~ local
network. The next section describes how to add a new external machine.

* External machines

| Machine    |   Address | Contact               |
|------------+-----------+-----------------------|
| overdrive1 |  10.0.0.3 | Ludovic Courtès       |
| dover      |  10.0.0.4 | Christopher Baines    |
| guix-x15   |  10.0.0.5 | Simon Josefsson       |
| guix-x15b  |  10.0.0.6 | Simon Josefsson       |
| guixp9     |  10.0.0.7 | Tobias Geerinckx-Rice |
| pankow     |  10.0.0.8 | Ricardo Wurmus        |
| kreuzberg  |  10.0.0.9 | Ricardo Wurmus        |
| grunewald  | 10.0.0.10 | Ricardo Wurmus        |
| bayfront   | 10.0.0.11 | Andreas Enge          |
| jade       | 10.0.0.12 | Chris Marusich        |
| sjd-p9     | 10.0.0.13 | Simon Josefsson       |
| lieserl    | 10.0.0.14 | Julien Lepiller       |

* Connect an external machine

 1. Install Guix System on the external machine. The Wireguard and Cuirass
    worker services must be properly configured.  The ~(sysadmin overdrive)~
    module can be used as an example.

    The Cuirass worker service should look like:

#+BEGIN_SRC scheme
    (service cuirass-remote-worker-service-type
               (cuirass-remote-worker-configuration
                (workers 2)
                (server "10.0.0.1:5555") ;berlin
                (substitute-urls '("http://10.0.0.1"))
                (systems '("armhf-linux" "aarch64-linux"))))
#+END_SRC

    The server field contains the Cuirass remote-server running on Berlin
    address. It must not be changed.

    The systems must be adjusted to the external machine supported
    architectures, emulated or not.

#+BEGIN_SRC scheme
  (service wireguard-service-type
           (wireguard-configuration
            (addresses (list "10.0.0.x/32"))
            (peers
             (list (wireguard-peer
                    (name "peer")
                    (endpoint "ci.guix.gnu.org:51820")
                    (public-key "wOIfhHqQ+JQmskRS2qSvNRgZGh33UxFDi8uuSXOltF0=")
                    (allowed-ips '("10.0.0.1/32"))
                    (keep-alive 25)))))
#+END_SRC

    The only field to be edited in the Wireguard service is the ~addresses~
    field.  It is the address of the new external machine on the Wireguard
    subnet.  An available address must be picked, see the *External machines*
    section above.  This section must be updated accordingly.

    The keep-alive at 25 secondes is important if the remote machine is
    sitting behind a NAT.  More details in the "NAT and Firewall Traversal
    Persistence" section of the Wireguard [[https://www.wireguard.com/quickstart/][quick start]] guide.

 2. Add the external machine Wireguard public key on Berlin. The new external
    machine public key can be obtained this way:

#+BEGIN_SRC bash
guix environment --ad-hoc wireguard-tools -- sudo -E wg
#+END_SRC

    The ~wireguard-service-type~ of *berlin.scm* file must be updated
    accordingly, by adding a new ~wireguard-peer~ this way;

#+BEGIN_SRC scheme
  (wireguard-peer
   (name "new-machine")
   (public-key "public-key")
   (allowed-ips '("10.0.0.x/32")))
#+END_SRC

 3. Add the external machine Guix signing key to ~hydra/keys/guix/berlin~
    directory. See [[https://guix.gnu.org/manual/en/html_node/Substitute-Server-Authorization.html][this]] page for more information.

 4. Make sure that the UDP port ~51820~ that is used by Wireguard is
    accessible on the external machine.  You might need to configure your ISP
    router NAT table accordingly.

    You can check that the external machine is accessible from berlin by
    running:

#+BEGIN_SRC bash
ssh 10.0.0.x
#+END_SRC

    The external machine should eventually appear [[https://ci.guix.gnu.org/workers][here]] and start building some
    packages.
