(use-modules (guix))

;;!begin-gexp-expansion
#~(list (string-append #$imagemagick "/bin/convert")
        (string-append #$emacs "/bin/emacs"))


⇒ (gexp (list (string-append (ungexp imagemagick)
                             "/bin/convert")
              (string-append (ungexp emacs)
                             "/bin/emacs")))

⇒ (let ((references
         (list (gexp-input imagemagick)
               (gexp-input emacs)))
        (proc (lambda (a b)
                (list 'list
                      (list 'string-append a
                            "/bin/convert")
                      (list 'string-append b
                            "/bin/emacs")))))
    (make-gexp references proc))

⇝ (list (string-append "/gnu/store/65qrc…-imagemagick-6.9"
                       "/bin/convert")
        (string-append "/gnu/store/825n3…-emacs-25.2"
                       "/bin/emacs"))
;;!end-gexp-expansion

;;!begin-gexp-hygiene
(let ((gen-body (lambda (x)
                  #~(let ((x 40))
                      (+ x #$x)))))
  #~(let ((x 2))
      #$(gen-body #~x))

⇝ (let ((x-1bd8-0 2))
    (let ((x-4f05-0 40)) (+ x-4f05-0 x-1bd8-0)))
;;!end-gexp-hygiene

