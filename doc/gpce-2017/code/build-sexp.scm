(use-modules (guix) (gnu))

(define imagemagick
  (specification->package "imagemagick"))

(with-store store
  (%guile-for-build (package-derivation store
                                        (specification->package "guile"))))

;!begin-build-sexp
(let* ((store (open-connection))
       (drv   (package-derivation store imagemagick))
       (image (add-to-store store "image.png" #t "sha256"
                            "./GuixSD.png"))
       (build
        '(let ((imagemagick (assoc-ref %build-inputs
                                       "imagemagick"))
               (image (assoc-ref %build-inputs "image")))
           (mkdir %output)
           (system* (string-append imagemagick "/bin/convert")
                    "-quality" "75%"
                    image
                    (string-append %output "/image.jpg")))))
  (build-expression->derivation store "example" build
                                #:inputs `(("imagemagick" ,drv)
                                           ("image" ,image))))
;!end-build-sexp

;!begin-imagemagick-gexp
(let* ((image (local-file "./GuixSD.png" "image.png"))
       (build #~(begin
                  (mkdir #$output)
                  (system* (string-append #$imagemagick
                                          "/bin/convert")
                           "-quality" "75%"
                           #$image
                           (string-append #$output "/image.jpg")))))
  (gexp->derivation "example" build))
;!end-imagemagick-gexp
