(with-imported-modules (source-module-closure
                        '((guix build utils)))
  #~(begin
      ;; Import the module in scope.
      (use-modules (guix build utils))

      ;; Use a function from (guix build utils).
      (mkdir-p #$output)))
