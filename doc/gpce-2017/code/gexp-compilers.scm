(define-gexp-compiler (package-compiler (package <package>)
                                        system target)
  ;; Compile PACKAGE to a derivation for SYSTEM, optionally
  ;; cross-compiled for TARGET.
  (if target
      (package->cross-derivation package target system)
      (package->derivation package system)))

(define-gexp-compiler (local-file-compiler (file <local-file>)
                                           system target)
  ;; "Compile" FILE by adding it to the store.
  (match file
    (($ <local-file> file name)
     (interned-file file name))))
