;;; Released under the GNU GPLv3 or any later version.
;;; Copyright © 2022 Ludovic Courtès <ludo@gnu.org>

;; Manifest to create an environment to build LaTeX documents.

;; Unfortunately we have resort to the monolithic 'texlive' package;
;; 'programming.cls' depends on way too many things.
(specifications->manifest '("texlive" "rubber"))
