# Patch review in Guix

## Impressions
Tom: patch review in GDB is different
Nicolo: why are they slow? do something from Emacs
Lars: automating, ML does not scale
Jonathan: automating, VMs to test patches, don't break master branch
Chris: reducing time between submit and commit of patches

## Process ATM
- People with commit access look at patches, review them and eventually commit them
- now on guix-patches@gnu.org list, before it was guix-devel@gnu.org
- debbugs is a patch backend, includes meta data (close, done, reopen)
- people can send feedback
- no "dedicated" maintainers for individual packages
- some patches never sent to guix-packages, instead directly committed to git

## Problems
- difficult to apply patches from mail
- patches breaking master get pushed
- long time for some patches to be reviewed
- sending patches to mailing list is a high barrier for newcomers
- dependencies between different patches/series
- sending a patch series can lead to one bug per patch, when done wrong

## Solutions
- submitting patches by pushing to a branch -> lots of people with commit access
  -> push to a branch on a "guix-staging" repo
  -> reviewer gets informed and can pull from it
- automatic tests (lint, build) after submit patches
- Chris shows his patchwork/laminar/Guix Data setup
  -> problems with patchwork
     -> git-hook required
     -> GDB tried to use patchwork -> too many patches
     -> hard to "parse" git things in e-mail
- gerrit
- how can we mix e-mail and git based review?
  -> UI to write comments to bugs
- just use Github/Gitlab
  -> difficult to keep track
  -> breaks existing work flows heavily
- "offline" review, in irc, together, once a month or so -> increasing motivation for reviewers
- gamification/badges for reviewing

## Small atomic actions
Jonathan: "read-only" branches of patches
Nicolo: git hook for sending the patches to debbugs
Tom: advertise Chris' stuff he as already
Chris: avoid push flooding

## What can we improve
- better process -> less people with commit access
- testing, checking the patches, commiting by robot (like Rust), robot picks reviewers randomly
- sign-up for reviews for particular packages/themes (e.g. R or GNOME)

## What others do
- GDB (patchwork, gerrit)
- Rust ("fixing" stuff on build farms VMs)
