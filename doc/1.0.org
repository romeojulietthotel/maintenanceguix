#+TITLE: Roadmap for Guix 1.0, 2018
#+SEQ_TODO: MAYBE TODO | DONE CANCELED
#+STARTUP: hidestars

* 'guix pull' & co.
** DONE 'guix pull' honors ~/.config/guix/channels.scm
   - State "DONE"       from "TODO"       [2018-09-02 Sun 17:12]
*** (guix channels) module provides easy way to build a set of channels
*** (guix inferior) uses that to allow interaction with an arbitrary Guix
** MAYBE 'guix pull' & commit authentication <https://bugs.gnu.org/22883>
** DONE 'guix package -m' (?) allows users to specify a Guix channel
** DONE Profile manifest entries record the channel instance they come from
** TODO build-self.scm trampoline runs faster
** DONE 'guix package --upgrade' reverses the order of packages <https://bugs.gnu.org/31142>
* UI/UX
** DONE Add colors for messages (error, warnings, hints, and possibly build logs)
** DONE Hide build logs in some UIs, as in ‘wip-ui’ branch & Cuirass
** MAYBE Rework grafts and profile hooks to run as “build continuations” <https://bugs.gnu.org/28310>
** TODO Hack something to display what needs to be built upfront (disable grafts)
** DONE Add ‘guix install’ alias
** DONE Add ‘guix system --delete-generations’
** DONE Polish & merge ‘wip-installer’
* core
** TODO Update & merge ‘wip-build-systems-gexp’
** MAYBE Merge ‘wip-gexp-hygiene’ if we have a portable way to compute gensyms
** DONE Use [[https://notabug.org/cwebber/guile-gcrypt][Guile-gcrypt]] instead of (guix gcrypt) & co.
** DONE Minimal bootstrap with Mes & co. for i686/x86_64 merged (‘wip-bootstrap’)
** MAYBE Use [[https://gitlab.com/rutger.van.beusekom/gash][Gash]] instead of Bash during bootstrap
* infrastructure
** DONE ci.guix.gnu.org points to berlin.guixsd.org
** DONE ci.guix.gnu.org is the default substitute server; hydra.gnu.org is deprecated
** DONE ARM build machines (+ qemu-binfmt) added behind berlin.guixsd.org
** DONE Tatiana's web UI deployed on berlin.guixsd.org
   - State "DONE"       from "TODO"       [2018-09-02 Sun 17:12]
** DONE Clément's Cuirass improvements deployed (inputs, non-blocking SQLite, etc.)
   - State "DONE"       from "TODO"       [2018-09-02 Sun 17:12]
** TODO web site available at guix.gnu.org
*** DNS already set up with two entries, but how to do deal with LE certs and all?
** DONE Mumi web UI available at patches.guix.gnu.org and bugs.guix.gnu.org
** DONE berlin.guixsd.org has big storage, uses a TTL > 60 days
** TODO Nar bandwidth issues on berlin fixed (nginx misconfiguration?)
* miscellaneous
** DONE “GuixSD” renamed to “Guix System”?
** TODO “Cuirass” renamed to “Guix CI”?
** DONE Use GDM in ‘%desktop-services’ (this will fix GNOME’s screen locker)
** TODO 'static-networking-service' supports IPv6
*** requires the use of the Netlink interface on the kernel Linux
*** libnl provides an API for this, with [[https://github.com/mordae/racket-rtnl/blob/master/rtnl/main.rkt][Racket bindings]]
*** or we could provide bindings for sendmsg(2) and 'struct msghdr'
*** short-term, we could shell out to "ip"
** DONE TeXlive profile hook <https://bugs.gnu.org/27217>
We’ll need a profile hook so people have the option to directly install
individual texlive packages in their profile.
** TODO Replace "texlive" dependency with minimal TeXlive unions
TeXlive is rather important for lots of users but also lots of packages.  Some
packages (e.g. "asymptote" and "mit-scheme") depend on the full TeXlive
distribution (5GB).  This is way too much for many users (low disk space, low
bandwidth, or simply lack of time), so we need to work out the set of strictly
required TeXlive packages for each package depending on TeXlive.
We need to package the missing individual TeXlive packages in those unions.
https://lists.gnu.org/archive/html/help-guix/2018-05/msg00218.html
** DONE Fix truncated/repeating utf-8 man pages <https://debbugs.gnu.org/cgi/bugreport.cgi?bug=30785>
