(use-modules (gnu))
(use-service-modules networking ssh monitoring web certbot guix)
(use-package-modules screen ssh zile linux certs)

(define %nginx-server-blocks
  (let ((common-locations
         (list
          (nginx-location-configuration
           (uri "= /nix-cache-info")
           (body '("
    return 200 'StoreDir: /gnu/store\nWantMassQuery: 0\nPriority: 100\n';
    add_header Content-Type text/plain;")))
          (nginx-location-configuration
           (uri "~ \\.narinfo$")
           (body '("
    proxy_pass http://nar-herder;

    # For HTTP pipelining.  This has a dramatic impact on performance.
    client_body_buffer_size 128k;

    # Narinfos requests are short, serve many of them on a connection.
    keepalive_requests 20000;
")))
          (nginx-location-configuration
           (uri "~ \\.narinfo/info$")
           (body '("proxy_pass http://nar-herder;")))
          (nginx-location-configuration
           (uri "/nar/")
           (body '("proxy_pass http://nar-herder;")))
          (nginx-location-configuration
           (uri "/file/")
           (body '("proxy_pass http://nar-herder;")))
          (nginx-named-location-configuration
           (name "nar-storage-location")
           (body '("rewrite /internal/(.*) /$1 break;"
                   "proxy_pass https://nar-storage;"
                   "
set  $via  \"1.1 bishan\";
if ($http_via) {
    set $via  \"$http_via, $via\";
}
proxy_set_header  Via  $via;"
                   "proxy_set_header Host hydra-guix-129.guix.gnu.org:443;")))
          (nginx-location-configuration
           (uri "= /latest-database-dump")
           (body '("proxy_pass http://nar-herder;")))
          (nginx-location-configuration
           (uri "= /recent-changes")
           (body '("proxy_pass http://nar-herder;")))
          (nginx-location-configuration
           (uri "= /metrics")
           (body '("proxy_pass http://nar-herder;")))
          (nginx-location-configuration
           (uri "~ ^/internal/nar/(.*)$")
           (body '("
    internal;
    root /var/lib/nars;

    try_files /nar/$1 @nar-storage-location;

    error_page 404 /404;

    client_body_buffer_size 256k;

    # Nars are already compressed.
    gzip off;
")))
          (nginx-location-configuration
           (uri "~ ^/internal/cached-nar/(.*)$")
           (body '("
    internal;
    root /var/cache/nar-herder;
    try_files /nar/$1 =404;

    error_page 404 /404;
    client_body_buffer_size 256k;
    gzip off;
")))
          (nginx-location-configuration
           (uri "~ ^/internal/database/(.*)$")
           (body '("internal;"
                   "alias /var/lib/nar-herder/$1;"))))))

    (list
     (nginx-server-configuration
      (server-name '("bishan.guix.gnu.org"))
      (listen '("80" "[::]:80"))
      (root (local-file "nginx/html/bishan" #:recursive? #t))
      (locations
       (append
        common-locations
        (list
         (nginx-location-configuration  ; For use by Certbot
          (uri "/.well-known")
          (body '(("root /var/www;"))))))))

      (nginx-server-configuration
       (server-name '("bishan.guix.gnu.org"))
       (listen '("443 ssl" "[::]:443 ssl"))
       (root (local-file "nginx/html/bishan" #:recursive? #t))
       (ssl-certificate
        "/etc/letsencrypt/live/bishan.guix.gnu.org/fullchain.pem")
       (ssl-certificate-key
        "/etc/letsencrypt/live/bishan.guix.gnu.org/privkey.pem")
       (raw-content
        '("
 # Make sure SSL is disabled.
 ssl_protocols       TLSv1.1 TLSv1.2 TLSv1.3;

 # Disable weak cipher suites.
 ssl_ciphers         HIGH:!aNULL:!MD5;
 ssl_prefer_server_ciphers on;"))
       (locations common-locations)))))

(operating-system
 (host-name "bishan")
 (timezone "Europe/Berlin")
 (locale "en_US.utf8")

 (bootloader (bootloader-configuration
              (bootloader grub-bootloader)
              (targets '("/dev/sda"))))

 (file-systems (cons (file-system
                      (device "/dev/sda2")
                      (mount-point "/")
                      (type "btrfs"))
                     %base-file-systems))

 (users (cons (user-account
               (name "cbaines")
               (group "users")

               (supplementary-groups '("wheel"
                                       "audio" "video")))
              %base-user-accounts))

 (packages (cons* screen zile btrfs-progs nss-certs %base-packages))

 (services
  (append
   (list
    (service static-networking-service-type
             (list
              (static-networking
               (addresses
                (list
                 (network-address
                  (device "enp1s0")
                  (value "5.9.22.61/32"))
                 (network-address
                  (device "enp1s0")
                  (ipv6? #t)
                  (value "2a01:4f8:161:123::/64"))))
               (routes
                (list
                 (network-route
                  (destination "default")
                  (device "enp1s0")
                  (gateway "5.9.22.33"))
                 (network-route
                  (destination "default")
                  (device "enp1s0")
                  (ipv6? #t)
                  (gateway "fe80::1"))))
               (name-servers
                '("2a01:4ff:ff00::add:1" "2a01:4ff:ff00::add:2")))))

    (service ntp-service-type)

    (service prometheus-node-exporter-service-type)

    (service certbot-service-type
             (certbot-configuration
              (certificates
               (list (certificate-configuration
                      (domains '("bishan.guix.gnu.org")))))
              (email "mail@cbaines.net")
              (webroot "/var/www")))

    (service nar-herder-service-type
             (nar-herder-configuration
              (mirror "https://bordeaux.guix.gnu.org")
              (storage "/var/lib/nars")
              (storage-limit 11615000000000) ; 10.8TiB
              (ttl "180d")
              (log-level 'INFO)))

    (service nginx-service-type
             (nginx-configuration
              (global-directives
               '((events . ((use . epoll)))
                 (worker_processes . 8)))
              (upstream-blocks
               (list (nginx-upstream-configuration
                      (name "nar-herder")
                      (servers '("127.0.0.1:8734")))
                     (nginx-upstream-configuration
                      (name "nar-storage")
                      (servers '("hydra-guix-129.guix.gnu.org:443")))))
              (server-blocks
               %nginx-server-blocks)))

    (service openssh-service-type
             (openssh-configuration
              (password-authentication? #f))))
   (modify-services %base-services
                    (guix-service-type
                     config => (guix-configuration
                                (extra-options
                                 (list "--max-jobs" "2"))))))))
