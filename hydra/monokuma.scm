;; GuixSD configuration file for the SoftIron OverDrive 1000 build machines.
;; Copyright © 2016, 2017, 2018 Ludovic Courtès <ludo@gnu.org>
;; Released under the GNU GPLv3 or any later version.

(use-modules (guix) (gnu))
(use-service-modules networking mcron ssh monitoring guix)
(use-package-modules screen ssh linux certs package-management)

(define (sysadmin name full-name)
  (user-account
   (name name)
   (comment full-name)
   (group "users")
   (supplementary-groups '("wheel" "kvm"))
   (home-directory (string-append "/home/" name))))

(define %accounts
  (list (sysadmin "ludo" "Ludovic Coutes")
        (sysadmin "rekado" "Ricardo Wurmus")
        (sysadmin "roptat" "Julien Lepiller")
        (sysadmin "dannym" "Danny Milosavljevic")
        (sysadmin "cbaines" "Christopher Baines")
        (sysadmin "lfam" "Leo Famulari")))

(define gc-job
  ;; Run 'guix gc' at 3AM every day.
  #~(job '(next-hour '(3)) "guix gc -F 200G"))

;; The actual machine.

(operating-system
  (host-name "monokuma")
  (timezone "Europe/London")
  (locale "en_US.UTF-8")

  (bootloader (bootloader-configuration
               (bootloader grub-efi-bootloader)
               (targets '("/boot/efi"))))
  (initrd-modules (cons* "xhci-pci" "ahci_platform" "sg" "sd_mod"
                         "ahci_dwc"
                         %base-initrd-modules))
  (file-systems (cons* (file-system
                         (device "/dev/sda2")
                         (mount-point "/")
                         (type "ext4"))
                       (file-system
                         (device "/dev/sda1")
                         (mount-point "/boot/efi")
                         ;; original options:
                         ;; (rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,errors=remount-ro)
                         (type "vfat"))
                       %base-file-systems))

  (swap-devices
   (list (swap-space
          (target "/dev/sda3"))))

  (users (append %accounts %base-user-accounts))
  (services (cons* (service openssh-service-type
                            (openssh-configuration
                             (password-authentication? #f)
                             (authorized-keys
                              `(("ludo" ,(local-file "keys/ssh/ludo.pub"))
                                ("rekado" ,(local-file "keys/ssh/rekado.pub"))
                                ("roptat" ,(local-file "keys/ssh/roptat.pub"))
                                ("lfam" ,(local-file "keys/ssh/lfam.pub"))
                                ("cbaines" ,(local-file "keys/ssh/cbaines.pub"))
                                ("dannym" ,(local-file "keys/ssh/dannym.pub"))))))
                   (service dhcp-client-service-type)
                   (service mcron-service-type
                            (mcron-configuration
                             (jobs (list gc-job))))

                   (service agetty-service-type
                            (agetty-configuration
                             (tty "ttyAMA0")
                             (keep-baud? #t)
                             (term "vt220")
                             (baud-rate "115200,38400,9600")))

                   (service ntp-service-type)
                   (service prometheus-node-exporter-service-type)

                   (service guix-build-coordinator-agent-service-type
                            (guix-build-coordinator-agent-configuration
                             (coordinator
                              "https://coordinator.bayfront.guix.gnu.org")
                             (authentication
                              (guix-build-coordinator-agent-password-file-auth
                               (uuid "5bbaa369-517d-49a1-9875-dc8fcf1e2e9f")
                               (password-file
                                "/etc/guix-build-coordinator-agent-password")))
                             (max-parallel-builds 4)
                             (systems '("aarch64-linux" "armhf-linux"))
                             (derivation-substitute-urls
                              (list "https://data.guix.gnu.org"
                                    "https://data.qa.guix.gnu.org"))
                             (non-derivation-substitute-urls
                              (list "https://bordeaux.guix.gnu.org"))))

                   (modify-services %base-services
                     (guix-service-type
                      config => (guix-configuration
                                 (inherit config)

                                 (substitute-urls
                                  '("https://bordeaux.guix.gnu.org"))

                                 (max-silent-time (* 12 3600))
                                 (timeout (* 48 3600))

                                 (authorized-keys
                                  (list
                                   (local-file
                                    "keys/guix/bordeaux.guix.gnu.org-export.pub")
                                   (local-file "keys/guix/data.guix.gnu.org.pub")
                                   (local-file "keys/guix/data.qa.guix.gnu.org.pub")))
                                 (extra-options
                                  '("--max-jobs=4")))))))

  (packages (cons* screen openssh strace nss-certs %base-packages)))
