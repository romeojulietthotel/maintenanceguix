#!/bin/sh
#
# Server-side Git update hook for checking whether pushed commits are
# signed.  To enable it, rename this file to 'update'.
#
# For more info:
#
# https://debbugs.gnu.org/cgi/bugreport.cgi?bug=22883#112
# https://savannah.nongnu.org/support/?109104
#
# TODO: Eventually, we'll check whether commits are signed by people
# authorized to modify the files they touch.

## $1 is the reference being revised
## $2 is the last HEAD
## $3 is the HEAD commit of the series of commits being applied
ref="$1"
rev_old="$2"
rev_new="$3"
span="`git rev-list ^$rev_old $rev_new`"
zero="0000000000000000000000000000000000000000"

result=0
for commit in $span
do
    if [ "$commit" = "$zero" ]
    then
	break
    fi

    if ! git cat-file -p "$commit" | grep -q '^gpgsig '
    then
	echo "error: commit '$commit' lacks an OpenPGP signature; rejected" >&2
	result=1
    fi
done

exit $result
