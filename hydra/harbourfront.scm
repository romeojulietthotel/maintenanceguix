(use-modules (gnu))
(use-service-modules mcron networking ssh guix)
(use-package-modules certs screen ssh vim)

(define gc-jobs
  (list #~(job '(next-hour  '(3)) "guix gc -F 200G")
        #~(job '(next-hour '(15)) "guix gc -F 100G")))

(operating-system
  (host-name "harbourfront")
  (timezone "Europe/Paris")
  (locale "en_US.utf8")

  (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (targets '("/dev/sda"))
               (terminal-outputs '(console))))

  (file-systems (cons (file-system
                        (device (file-system-label "my-root"))
                        (mount-point "/")
                        (type "ext4"))
                      %base-file-systems))

  (swap-devices
   (list (swap-space
          (target "/swapfile"))))

  ;; Add kernel module for Symbios Logic SAS1068E
  (initrd-modules (append (list "mptsas")
                          %base-initrd-modules))

  (users (cons* (user-account
                 (name "andreas")
                 (comment "Andreas Enge")
                 (group "users")
                 (supplementary-groups '("wheel"))
                 (home-directory "/home/andreas"))
                (user-account
                 (name "cbaines")
                 (comment "Christopher Baines")
                 (group "users")
                 (supplementary-groups '("wheel"))
                 (home-directory "/home/cbaines"))
                (user-account
                 (name "bayfront")
                 (comment "Bayfront Offload")
                 (group "users")
                 (home-directory "/home/bayfront"))
                %base-user-accounts))

  ;; Globally-installed packages.
  (packages (cons* nss-certs screen openssh vim %base-packages))

  (services (cons* (service static-networking-service-type
                     (list
                       (static-networking
                         (addresses
                           (list
                             (network-address
                               (device "enp12s0f0")
                               (value "147.210.17.78/23"))))
                         (routes
                           (list
                             (network-route
                               (destination "default")
                               (gateway "147.210.17.254"))))
                         (name-servers
                           '("147.210.245.1"
                             "147.210.245.11"
                             "147.210.245.111")))))
                   (service openssh-service-type
                     (openssh-configuration
                       (permit-root-login 'prohibit-password)
                       (authorized-keys
                         `(("andreas" ,(local-file "keys/ssh/andreas.pub"))
                           ("cbaines" ,(local-file "keys/ssh/cbaines.pub"))
                           ("bayfront" ,(local-file "keys/ssh/bayfront.pub"))
                           ("root" ,(local-file "keys/ssh/andreas.pub"))))))
                   (service mcron-service-type
                            (mcron-configuration
                             (jobs gc-jobs)))
                   (service ntp-service-type)

                   (service guix-build-coordinator-agent-service-type
                            (guix-build-coordinator-agent-configuration
                             (coordinator
                              "https://coordinator.bayfront.guix.gnu.org")
                             (authentication
                              (guix-build-coordinator-agent-password-file-auth
                               (uuid "7470cbd5-f64e-496c-a57b-593543ee4686")
                               (password-file
                                "/etc/guix-build-coordinator-agent-password")))
                             (max-parallel-builds 8)
                             (max-1min-load-average 8)
                             (max-allocated-builds 48)
                             (systems '("x86_64-linux" "i686-linux"))
                             (derivation-substitute-urls
                              (list "https://data.guix.gnu.org"
                                    "https://data.qa.guix.gnu.org"))
                             (non-derivation-substitute-urls
                              (list "https://bordeaux.guix.gnu.org"))))

                   (modify-services %base-services
                     (guix-service-type config =>
                                        (guix-configuration
                                         (substitute-urls
                                          '("https://bordeaux.guix.gnu.org"))

                                         (max-silent-time (* 24 3600))
                                         (timeout (* 48 3600))

                                         (authorized-keys
                                          (list
                                           (local-file "keys/guix/bordeaux.guix.gnu.org-export.pub")
                                           (local-file "keys/guix/data.guix.gnu.org.pub")
                                           (local-file "keys/guix/data.qa.guix.gnu.org.pub")))))))))
