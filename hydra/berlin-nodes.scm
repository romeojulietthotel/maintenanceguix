;; "guix deploy" configuration file for the build machines of the compile
;; farm hosted at the MDC, Berlin.

(use-modules (sysadmin build-machines)
             (sysadmin people)
             (srfi srfi-1)
             (ice-9 match))

(define nodes
  '((101
     "141.80.167.158"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGqLy+LVndyfuzwZmln/nrHylAN7FotSmso9kZaYPpzo"
     128)
    (102
     "141.80.167.159"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEq4YoQHA0ShXIVbk7E4Jh4KZRPrt1EN9DYniraR8oYj"
     128)
    (103
     "141.80.167.160"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICiFpDx+NIVHD4ffZotDyJDdEiwo8Cy8fAQU6cLt6mT/"
     128)
    (104
     "141.80.167.161"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINuVkwaeU+ddDpDQoxyFboiBnRNyhGDT8yOy8VAyJxZ6"
     128)
    (105
     "141.80.167.162"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIH9o9VrkR2OKoGeuyJkzSsLIaDVApkbHEQvgr8aywQf8"
     128)
    (106
     "141.80.167.163"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBWN8i6YSGaRddTUgjodvQ4+g+6qYRe+0t9Mi8zOXawG"
     128)
    (107
     "141.80.167.164"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAII+nI0XnLKShi3tZEdPdEVQ1VLlZjgQNSKMTK55FwH/4"
     128)
    (108
     "141.80.167.165"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHvMT+OlslyCzp7PvIvG/m9aCNhk3jnGS4kh8Cxh26CK"
     128)
    (109
     "141.80.167.166"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHkmH+o9P2kmgtjyGU9/vLEmFbxwUlq62lWu3lLc1J5o"
     128)
    (110
     "141.80.167.167"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM2A2GxCw3oF6W2a5P9/K/jw1BWNJdAy9cr7NLRWvHVl"
     128)
    (111
     "141.80.167.168"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILJoZitLeltTfd7dDAnRbuP1uCWmTsYjIKALcadXknMl"
     128)
    (112
     "141.80.167.169"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFS6PDp6MVutJiieJgDaLvub83oeTvWYLJnELxqCyO7x"
     128)
    (113
     "141.80.167.170"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMn5ujp4uTRVwYGPr2kgh7YMXISj+WyRxe8cGxzb1KrL"
     128)
    (114
     "141.80.167.171"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIF4ST+J7Xdmrft+sD1HEOAjADA+QZ+hMXRV3PnN0Rs+A"
     128)
    (115
     "141.80.167.172"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG9zXGZ5b6QroN4RybnKLIMZwKtFuMpsNypkUXdFmH88"
     128)
    (116
     "141.80.167.173"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFg0y4UyuTkYoa3hwqj2ByQXYBMQdbPKz7nEz7I1lquL"
     128)
    (117
     "141.80.167.174"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINPPjhX6Z3bgt7EZmIfUdsgFnqp3yLr4msccjwsD2Q8F"
     128)
    (118
     "141.80.167.175"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJfJfTctnzEzVBLZxIq4WIOWY0s9JHcvIztdIYSFlklH"
     128)
    (119
     "141.80.167.176"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGE6FwL94/YDJIioQsLqh/MnwGcXmKYARd/kBGs+RWM7"
     128)
    (120
     "141.80.167.177"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIH3rXQZCQbVliJUgavSxNWvA4XUX7cXj7zd5VvUggCbv"
     128)
    (121
     "141.80.167.178"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGNVaPCyKRrprBivEWYmtVecaJ+DIkET3gCYzGOuRAcz"
     128)
    (122
     "141.80.167.179"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHladb6HkAEmITzNOmI1kH7A4R1MiKp0Y72aPJNwuIDB"
     128)
    (123
     "141.80.167.180"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOM29Lj7rNDDsU5JOuDgFGfepWY9WHs6WaMLj9/7IceX"
     128)
    (124
     "141.80.167.181"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIODiKP7qIkkDeqvzKG2JsrDlNRe3CTN+icGgQ1J5ZUP+"
     128)
    ;; This is the former head node.  What used to be node 125 is now
    ;; the new head.
    (125
     "141.80.167.182"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPrlasUtgZgKfJ0oNhBQx/2QIQ+J+jbAT842VoJlBhor"
     256)
    (126
     "141.80.167.183"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIUprP1E2cRkMrwBnl1FkeCQ5UhZRin6dKQrB9p4WrV6"
     192)
    (127
     "141.80.167.184"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHdrEcEoo2AQ6aDXhLUWxLhp4kTq+DJLwXxvgu4As1bo"
     192)
    (128
     "141.80.167.185"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAIomlYpFmdaTiWGf4DWs6sc831zbNlU5XBjicHmZINA"
     192)
    ;; Node 129 is not listed here, as it's used as Berlin's twin and
    ;; has its own configuration file, deploy-node-129.scm.
    (130
     "141.80.167.187"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICZilog+9Jdim9k07baYK6QZfkZRZbQQriExjtOEfjQ5"
     192)))

(define %authorized-guix-keys
  ;; List of authorized 'guix archive' keys.
  (list (local-file "keys/guix/berlin.guixsd.org-export.pub")))

(map (match-lambda
       ((id ip host-key memory)
        (machine
         (operating-system
           (berlin-new-build-machine-os id
                                        #:authorized-guix-keys
                                        %authorized-guix-keys
                                        #:emulated-architectures
                                        '("ppc64le")
                                        #:childhurd? (childhurd-ip? ip)
                                        #:systems
                                        (let ((lst '("x86_64-linux"
                                                     "i686-linux")))
                                          ;; Machines with a childhurd
                                          ;; offload *-gnu builds to their
                                          ;; local childhurd, transparently.
                                          (if (childhurd-ip? ip)
                                              (cons "i586-gnu" lst)
                                              lst))
                                        #:max-jobs 4
                                        ;; The big-memory machines have 96
                                        ;; logical cores, the others 64.
                                        #:max-cores (if (> memory 128)
                                                        24 16)))
         (environment managed-host-environment-type)
         (configuration (machine-ssh-configuration
                         (system "x86_64-linux")
                         (host-name (format #f "141.80.167.~d" (+ id 57)))
                         (host-key host-key))))))
     nodes)
