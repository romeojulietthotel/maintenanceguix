;; Guix System configuration file for a SolidRun Honeycomb LX2 machine
;; Copyright © 2016, 2017, 2018 Ludovic Courtès <ludo@gnu.org>
;; Copyright © 2021, 2022 Christopher Baines <mail@cbaines.net>
;; Released under the GNU GPLv3 or any later version.

(use-modules (guix) (gnu))
(use-service-modules networking mcron ssh monitoring guix web certbot)
(use-package-modules screen ssh linux certs)

(define (sysadmin name full-name)
  (user-account
   (name name)
   (comment full-name)
   (group "users")
   (supplementary-groups '("wheel" "kvm"))
   (home-directory (string-append "/home/" name))))

(define %accounts
  (list (sysadmin "cbaines" "Christopher Baines")
        (sysadmin "ludo" "Ludovic Courtès")))

(define %default-extra-linux-options
  (@@ (gnu packages linux) %default-extra-linux-options))
(define make-linux-libre*
  (@@ (gnu packages linux) make-linux-libre*))

(define-public linux-libre-arm64-honeycomb
  (make-linux-libre* linux-libre-version
                     linux-libre-gnu-revision
                     linux-libre-source
                     '("aarch64-linux")
                     #:extra-version "arm64-generic"
                     #:extra-options
                     (append
                      `(;; needed to fix the RTC on rockchip platforms
                        ("CONFIG_RTC_DRV_RK808" . #t)
                        ;; Pinebook display, battery, charger and usb
                        ("CONFIG_DRM_ANALOGIX_ANX6345" . m)
                        ("CONFIG_CHARGER_AXP20X" . m)
                        ("CONFIG_INPUT_AXP20X_PEK" . m)
                        ("CONFIG_CHARGER_AXP20X" . m)
                        ("CONFIG_BATTERY_AXP20X" . m)
                        ("CONFIG_PINCTRL_AXP209" . m)
                        ("CONFIG_AXP20X_POWER" . m)
                        ("CONFIG_AXP20X_ADC" . m)

                        ("CONFIG_GPIO_SYSFS" . #t)
                        ("CONFIG_GPIO_MPC8XXX" . #t)
                        ("CONFIG_NET_PKTGEN" . #t)
                        ("CONFIG_USB_SERIAL" . #t)
                        ("CONFIG_USB_SERIAL_CONSOLE" . #t)
                        ("CONFIG_USB_SERIAL_GENERIC" . #t)
                        ("CONFIG_USB_SERIAL_SIMPLE" . #t)
                        ("CONFIG_USB_SERIAL_FTDI_SIO" . #t)
                        ("CONFIG_USB_ACM" . #t)
                        ("CONFIG_USB_NET_DRIVERS" . #t)
                        ("CONFIG_USB_USBNET" . #t)
                        ("CONFIG_USB_NET_CDCETHER" . #t)
                        ("CONFIG_USB_NET_CDC_NCM" . #t)
                        ("CONFIG_USB_NET_NET1080" . #t)
                        ("CONFIG_USB_NET_CDC_SUBSET_ENABLE" . #t)
                        ("CONFIG_USB_NET_CDC_SUBSET" . #t)
                        ("CONFIG_USB_ARMLINUX" . #t)
                        ("CONFIG_BLK_DEV_NVME" . #t)
                        ("CONFIG_NVMEM_BCM_OCOTP" . #t)
                        ("CONFIG_DRM_AMDGPU" . #t)
                        ("CONFIG_DRM_AMDGPU_SI" . #t)
                        ("CONFIG_DRM_AMDGPU_CIK" . #t)
                        ("CONFIG_DRM_AMDGPU_USERPTR" . #t)
                        ("CONFIG_DRM_AMD_DC" . #t)
                        ("CONFIG_CHASH" . #t)
                        ("CONFIG_PMBUS" . #t)
                        ("CONFIG_SENSORS_PMBUS" . #t)
                        ("CONFIG_REGULATOR" . #t)
                        ("CONFIG_REGULATOR_FIXED_VOLTAGE" . #t)
                        ("CONFIG_REGULATOR_PWM" . #t)
                        ("CONFIG_SENSORS_AMC6821" . #t)
                        ("CONFIG_SENSORS_LM90" . #t)
                        ("CONFIG_SENSORS_LTC2978" . #t)
                        ("CONFIG_SENSORS_LTC2978_REGULATOR" . #t)
                        ("CONFIG_TMPFS" . #t)
                        ("CONFIG_TMPFS_POSIX_ACL" . #t)
                        ("CONFIG_TMPFS_XATTR" . #t)
                        ("CONFIG_BLK_DEV_RAM_SIZE=5242" . #t)
                        ("CONFIG_POWER_RESET_GPIO" . #t)
                        ("CONFIG_CRYPTO_USER_API_HASH" . #t)
                        ("CONFIG_CRYPTO_USER_API_SKCIPHER" . #t)
                        ("CONFIG_CRYPTO_USER_API_RNG" . #t)
                        ("CONFIG_CRYPTO_USER_API_AEAD" . #t)

                        ;; Pinebook PRO battery and sound support
                        ("CONFIG_BATTERY_CW2015" . m)
                        ("CONFIG_CHARGER_GPIO" . m)
                        ("CONFIG_SND_SOC_ES8316" . m))
                      %default-extra-linux-options)))

(operating-system
  (host-name "hamal")
  (timezone "Europe/London")
  (locale "en_US.utf8")

  (bootloader (bootloader-configuration
               (bootloader grub-efi-bootloader)
               (targets '("/boot/efi"))))

  (kernel linux-libre-arm64-honeycomb)
  (initrd-modules '())
  (kernel-arguments '("arm-smmu.disable_bypass=0"
                      "iommu.passthrough=1"))

  (file-systems (cons* (file-system
                         (device "/dev/nvme0n1p2")
                         (mount-point "/")
                         (type "ext4"))
                       (file-system
                         (device "/dev/nvme0n1p1")
                         (mount-point "/boot/efi")
                         (type "vfat"))
                       %base-file-systems))

  (swap-devices (list (swap-space (target "/swapfile"))))

  (users (append %accounts %base-user-accounts))
  (packages
   (cons* screen
          nss-certs
          %base-packages))
  (services
   (cons* (service dhcp-client-service-type)

          (service mcron-service-type
                   (mcron-configuration
                    (jobs
                     (list #~(job '(next-hour '(3)) "guix gc -F 250G")))))

          (service agetty-service-type
                   (agetty-configuration
                    (tty "ttyAMA0")
                    (keep-baud? #t)
                    (term "vt220")
                    (baud-rate "115200,38400,9600")))

          (service openssh-service-type
                   (openssh-configuration
                    (password-authentication? #f)))

          (service ntp-service-type)
          (service prometheus-node-exporter-service-type)

          (service guix-build-coordinator-agent-service-type
                   (guix-build-coordinator-agent-configuration
                    (coordinator "https://coordinator.bayfront.guix.gnu.org")
                    (authentication
                     (guix-build-coordinator-agent-password-file-auth
                      (uuid "d9ed2db6-ad3c-45f7-92af-81ad973d5de0")
                      (password-file
                       "/etc/guix-build-coordinator-agent-password")))
                    (max-parallel-builds 8)
                    (systems '("aarch64-linux" "armhf-linux"))
                    (derivation-substitute-urls
                     (list "https://data.guix.gnu.org"
                           "https://data.qa.guix.gnu.org"))
                    (non-derivation-substitute-urls
                     (list "https://bordeaux.guix.gnu.org"))))

          (modify-services %base-services
            (guix-service-type
             config => (guix-configuration
                        (inherit config)
                        (substitute-urls
                         '("https://bordeaux.guix.gnu.org"))

                        (max-silent-time (* 12 3600))
                        (timeout (* 72 3600))

                        (build-accounts 32)

                        (authorized-keys
                         (list
                          (local-file "keys/guix/bordeaux.guix.gnu.org-export.pub")
                          (local-file "keys/guix/data.guix.gnu.org.pub")
                          (local-file "keys/guix/data.qa.guix.gnu.org.pub")))))))))
