;; Install this file as /etc/guix/machines.scm on berlin.guix.gnu.org

(use-modules (ice-9 match) (srfi srfi-1))

;; These are all hosted at the MDC in Berlin Buch.  They are connected to
;; a dedicated VLAN and can only be accessed from berlin.guix.gnu.org.
(define hosts
  '(;;; New machines.  We should use DNS for them in the future.
    ;; hydra-guix-101
    ("141.80.167.158"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGqLy+LVndyfuzwZmln/nrHylAN7FotSmso9kZaYPpzo"
     128)
    ;; hydra-guix-102
    ("141.80.167.159"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEq4YoQHA0ShXIVbk7E4Jh4KZRPrt1EN9DYniraR8oYj"
     128)
    ;; hydra-guix-103
    ("141.80.167.160"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICiFpDx+NIVHD4ffZotDyJDdEiwo8Cy8fAQU6cLt6mT/"
     128)
    ;; hydra-guix-104
    ("141.80.167.161"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINuVkwaeU+ddDpDQoxyFboiBnRNyhGDT8yOy8VAyJxZ6"
     128)
    ;; hydra-guix-105
    ("141.80.167.162"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIH9o9VrkR2OKoGeuyJkzSsLIaDVApkbHEQvgr8aywQf8"
     128)
    ;; hydra-guix-106
    ("141.80.167.163"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBWN8i6YSGaRddTUgjodvQ4+g+6qYRe+0t9Mi8zOXawG"
     128)
    ;; hydra-guix-107
    ("141.80.167.164"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAII+nI0XnLKShi3tZEdPdEVQ1VLlZjgQNSKMTK55FwH/4"
     128)
    ;; hydra-guix-108
    ("141.80.167.165"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHvMT+OlslyCzp7PvIvG/m9aCNhk3jnGS4kh8Cxh26CK"
     128)
    ;; hydra-guix-109
    ("141.80.167.166"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHkmH+o9P2kmgtjyGU9/vLEmFbxwUlq62lWu3lLc1J5o"
     128)
    ;; hydra-guix-110
    ("141.80.167.167"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM2A2GxCw3oF6W2a5P9/K/jw1BWNJdAy9cr7NLRWvHVl"
     128)
    ;; hydra-guix-111
    ("141.80.167.168"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILJoZitLeltTfd7dDAnRbuP1uCWmTsYjIKALcadXknMl"
     128)
    ;; hydra-guix-112
    ("141.80.167.169"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFS6PDp6MVutJiieJgDaLvub83oeTvWYLJnELxqCyO7x"
     128)
    ;; hydra-guix-113
    ("141.80.167.170"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMn5ujp4uTRVwYGPr2kgh7YMXISj+WyRxe8cGxzb1KrL"
     128)
    ;; hydra-guix-114
    ("141.80.167.171"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIF4ST+J7Xdmrft+sD1HEOAjADA+QZ+hMXRV3PnN0Rs+A"
     128)
    ;; hydra-guix-115
    ("141.80.167.172"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG9zXGZ5b6QroN4RybnKLIMZwKtFuMpsNypkUXdFmH88"
     128)
    ;; hydra-guix-116
    ("141.80.167.173"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFg0y4UyuTkYoa3hwqj2ByQXYBMQdbPKz7nEz7I1lquL"
     128)
    ;; hydra-guix-117
    ("141.80.167.174"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINPPjhX6Z3bgt7EZmIfUdsgFnqp3yLr4msccjwsD2Q8F"
     128)
    ;; hydra-guix-118
    ("141.80.167.175"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJfJfTctnzEzVBLZxIq4WIOWY0s9JHcvIztdIYSFlklH"
     128)
    ;; hydra-guix-119
    ("141.80.167.176"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGE6FwL94/YDJIioQsLqh/MnwGcXmKYARd/kBGs+RWM7"
     128)
    ;; hydra-guix-120
    ("141.80.167.177"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIH3rXQZCQbVliJUgavSxNWvA4XUX7cXj7zd5VvUggCbv"
     128)
    ;; hydra-guix-121
    ("141.80.167.178"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGNVaPCyKRrprBivEWYmtVecaJ+DIkET3gCYzGOuRAcz"
     128)
    ;; hydra-guix-122
    ("141.80.167.179"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHladb6HkAEmITzNOmI1kH7A4R1MiKp0Y72aPJNwuIDB"
     128)
    ;; hydra-guix-123
    ("141.80.167.180"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOM29Lj7rNDDsU5JOuDgFGfepWY9WHs6WaMLj9/7IceX"
     128)
    ;; hydra-guix-124
    ("141.80.167.181"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIODiKP7qIkkDeqvzKG2JsrDlNRe3CTN+icGgQ1J5ZUP+"
     128)
    ;; hydra-guix-125
    ("141.80.167.182"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPrlasUtgZgKfJ0oNhBQx/2QIQ+J+jbAT842VoJlBhor"
     192)
    ;; hydra-guix-126
    ("141.80.167.183"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIUprP1E2cRkMrwBnl1FkeCQ5UhZRin6dKQrB9p4WrV6"
     192)
    ;; hydra-guix-127
    ("141.80.167.184"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHdrEcEoo2AQ6aDXhLUWxLhp4kTq+DJLwXxvgu4As1bo"
     192)
    ;; hydra-guix-128
    ("141.80.167.185"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAIomlYpFmdaTiWGf4DWs6sc831zbNlU5XBjicHmZINA"
     192)
    ;; hydra-guix-129
    ("141.80.167.186"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMuCdrMoF25T9ejPLAAcS92b6lVIz5+U0avyYPQTG5NI"
     192)
    ;; hydra-guix-130
    ;; FIXME: Disabled Nov 19 2022; waiting troubleshooting from
    ;; Madalin (segfaults in libc).
    ;; ("141.80.167.187"
    ;;  "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICZilog+9Jdim9k07baYK6QZfkZRZbQQriExjtOEfjQ5"
    ;;  192)
    ))

(define template-x86_64
  (match-lambda
    ;; Prefer building on the new nodes.
    ((ip key 128)
     (build-machine
      (name ip)
      (user "hydra")
      (systems
       ;; Some of these machines run a childhurd, which they offload to (we
       ;; effectively have two-level offloading, then).
       (append (if (childhurd-ip? ip)
                   '("i586-gnu")
                   '())
               '("x86_64-linux" "i686-linux")))
      (host-key key)
      (compression "no")
      (speed 3)                                   ;don't increase it too much
                                        ;or everything goes there
      (parallel-builds 8)))
    ((name key 192)
     (build-machine
      (name name)
      (user "hydra")
      (systems '("x86_64-linux" "i686-linux"))
      (host-key key)
      (compression "no")
      (speed 3)                                   ;don't increase it too much
                                        ;or everything goes there
      (parallel-builds 8)))
    ((ip key ram)
     (build-machine
      (name ip)
      (user "hydra")
      (systems '("x86_64-linux" "i686-linux"))
      (host-key key)
      (compression "no")
      (speed 2)
      (parallel-builds 2)))))

(define (aarch64->armhf machine)
  (build-machine
   (inherit machine)
   (systems '("armhf-linux"))
   (speed .9)
   (parallel-builds 1)))            ;limit to favor the "real" ARMv7 machines

(define (x86_64->qemu-armhf machine)
  (build-machine
   (inherit machine)
   (systems '("armhf-linux"))
   (speed .8)                       ;prefer the "native" AArch64 machines
   (parallel-builds 1)))

(define (x86_64->qemu-aarch64 machine)
  (build-machine
   (inherit machine)
   (systems '("aarch64-linux"))
   (speed .9)
   (parallel-builds 1)))

(define overdrive
  ;; The SoftIron OverDrive 1000 donated by ARM:
  ;; CPU: AMD A1100 (4 Cortex A57 cores)
  ;; RAM: 8 GB
  (list (build-machine
         ;;overdrive1
         (name "10.0.0.3")
         (user "hydra")
         (overload-threshold 1.2)
         (systems '("aarch64-linux" "armhf-linux"))
         (host-key
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPf2f93c90oi9s9qGVGWC3sDgG7kEBvIEwR021NsfG+z root@overdrive")
         (parallel-builds 2))

        ;; 2022-02-17: cannot be reached.
        #;
        (build-machine
         ;;dover
         (name "10.0.0.4")
         (user "hydra")
         (overload-threshold 1.2)
         (systems '("aarch64-linux" "armhf-linux"))
         (host-key
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJLRYD5RXZ3Espe+Kv1SzZl8Qc3NZ356Bq+cGjnKsDHY root@linux")
         (parallel-builds 2))))

(define honeycomb
  ;; SolidRun LX2160A Honeycomb
  ;; CPU: 16 ARM Cortex-A72 cores
  ;; RAM: 32 GB
  (list (build-machine
         ;;pankow
         (name "10.0.0.8")
         (user "hydra")
         (overload-threshold 1.2)
         (speed 2.0)                    ; prefer over overdrives
         (systems '("aarch64-linux" "armhf-linux"))
         (host-key
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMiOfBwh78K3KNEV1ZQf0pyVtYFSoLgWryMMy0GdMJ0H")
         (parallel-builds 4))
        (build-machine
         ;;kreuzberg
         (name "10.0.0.9")
         (user "hydra")
         (overload-threshold 1.2)
         (speed 2.0)                    ; prefer over overdrives
         (systems '("aarch64-linux" "armhf-linux"))
         (host-key
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFjixKdkTBoEUflxX/n/flhg7GoqbfkfoVrhD0GROZxl")
         (parallel-builds 4))
        (build-machine
         ;;grunewald
         (name "10.0.0.10")
         (user "hydra")
         (overload-threshold 1.2)
         (speed 2.0)                    ; prefer over overdrives
         (systems '("aarch64-linux" "armhf-linux"))
         (host-key
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIB9slskCGIBFwRRzsWmePIsMJ8W1muqvDIgPG3xQeu6")
         (parallel-builds 4))))

(define armv7
  (list
   ;; BeagleBoard X15 kindly hosted by Simon Josefsson.
   ;; CPU: Cortex A15 (2 cores)
   ;; RAM: 2 GB
   ;; 2022-02-17: cannot be reached.
   #;
   (build-machine
    (name "10.0.0.5")                   ;guix-x15
    (user "hydra")
    (systems '("armhf-linux"))
    (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOfXjwCAFWeGiUoOVXEgtIeXxbtymjOTg7ph1ObMAcJ0 root@beaglebone"))

   ;; 2022-02-17: cannot be reached.
   #;
   (build-machine
    (name "10.0.0.6")                   ;guix-x15b
    (user "hydra")
    (systems '("armhf-linux"))
    (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJEbCOTTg9Tl0E23Mnc0UA4Ib2W5oDqTukk6mT98tOph root@beaglebone"))

   #;(build-machine
   (name "hydra-slave1.netris.org")
   (port 7275)
   (user "hydra")
   (systems '("armhf-linux"))
   (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPDAe9mXufZXFfFlezafA/G2Nng66ssLLi5xPP+9NhGm root@hydra-slave1")
   (speed 1.0)
   (parallel-builds 2))

   ;; I/O errors as of 2020-02-27
   #;(build-machine
   (name "hydra-slave2.netris.org")
   (port 7276)
   (user "hydra")
   (systems '("armhf-linux"))
   (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHzlJZzZfPiEcehmLFtQVYVt3j9w4DHPL6YgSC3EHJK+ root@hydra-slave2")
   (speed 1.0)
   (parallel-builds 2))

   ;; Not responding as of 2020-02-27
   #;(build-machine
   (name "hydra-slave3.netris.org")
   (port 7274)
   (user "hydra")
   (systems '("armhf-linux"))
   (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBLOVhnS24E+Z1bXLUU4z4gE5+HsFxDxUPA6MbLHmnME root@hydra-slave3")
   (speed 1.0)
   (parallel-builds 2))))

(define powerpc64le
  (list
   ;; guixp9 - A VM donated/hosted by OSUOSL & administered by nckx.
   ;; 8 POWER9 2.2 (pvr 004e 1202) cores, 16 GiB RAM, 160 GB storage.
   (build-machine
    (name "10.0.0.7")
    (user "hydra")
    (systems '("powerpc64le-linux"))
    (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJEbRxJ6WqnNLYEMNDUKFcdMtyZ9V/6oEfBFSHY8xE6A nckx"))
   ;; sjd-p9 - A VM donated/hosted by Simon Joseffson, but blame nckx for any problems.
   ;; 32 POWER9 2.3 (pvr 004e 1203) cores, 64 GiB RAM, 16 GB / + 256 GB /gnu storage.
   (build-machine
    (parallel-builds 16)
    (speed 4.0)
    (name "10.0.0.13")
    (user "hydra")
    (systems '("powerpc64le-linux"))
    (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMUkktI2HAycb4nqWwVBn5OCe5dyF4pbjqvyPTICz/9A nckx"))))

(define build-machine-name
  (@@ (guix scripts offload) build-machine-name))

(define (childhurd-ip? ip)        ;XXX: copied from (sysadmin build-machines)
  "Return #t if IP should be running a Childhurd."
  (member ip '("141.80.167.158" "141.80.167.159"
               "141.80.167.160" "141.80.167.161")))

(let* ((x86_64 (map template-x86_64 hosts)))
  (append overdrive
          honeycomb
          ;; This has been disabled until bug# 43513 is fixed.
          ;;(map aarch64->armhf overdrive)
          armv7
          powerpc64le
          x86_64
          ;; This has been disabled until bug# 43513 is fixed.
          ;;(map x86_64->qemu-armhf fast)
          ))
