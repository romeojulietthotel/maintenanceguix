(use-modules (gnu)
             (sysadmin people))
(use-service-modules desktop networking ssh xorg monitoring mcron guix)

(define %sysadmins
  (list (sysadmin (name "cbaines")
                  (full-name "Christopher Baines")
                  (ssh-public-key (local-file "keys/ssh/cbaines.pub")))
        (sysadmin (name "ludo")
                  (full-name "Ludovic Courtès")
                  (ssh-public-key (local-file "keys/ssh/ludo.pub")))
        (sysadmin (name "mathieu")
                  (full-name "Mathieu Othacehe")
                  (ssh-public-key (local-file "keys/ssh/mathieu.pub")))))

(operating-system
  (host-name "fosshost1")

  (locale "en_US.utf8")
  (timezone "America/New_York")
  (keyboard-layout (keyboard-layout "us"))

  (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (target "/dev/sda")
               (keyboard-layout keyboard-layout)))
  (initrd-modules (append '("virtio_scsi")
                          %base-initrd-modules))
  (swap-devices (list "/dev/sda2" "/swapfile"))

  (file-systems (cons* (file-system
                         (mount-point "/")
                         (device
                          (uuid "f2659154-1725-42bf-b29d-5fd11fc649c0"
                                'ext4))
                         (type "ext4"))
                       %base-file-systems))

  (users %base-user-accounts)

  (packages (append
             (list (specification->package "nss-certs"))
             %base-packages))

  (services
   (append
    (list (service openssh-service-type
                   (openssh-configuration
                    (password-authentication? #f)))
          (service sysadmin-service-type %sysadmins)
          (static-networking-service "eth0"
                                     "147.75.35.153"
                                     #:netmask "255.255.255.0"
                                     #:gateway "147.75.35.145"
                                     #:name-servers '("8.8.8.8"))
          (service ntp-service-type)
          (service prometheus-node-exporter-service-type)
          (service mcron-service-type
                   (mcron-configuration
                    (jobs (list #~(job "0 1 * * *"
                                       "guix gc -F 200G")))))
          (service guix-build-coordinator-agent-service-type
                   (guix-build-coordinator-agent-configuration
                    (coordinator "https://coordinator.guix-patches.cbaines.net")
                    (authentication
                     (guix-build-coordinator-agent-password-file-auth
                      (uuid "b35e828a-0229-4cbc-9e41-cf78fe4116fb")
                      (password-file
                       "/etc/guix-build-coordinator-agent-password")))
                    (max-parallel-builds 4)
                    (systems '("x86_64-linux" "i686-linux"))
                    (derivation-substitute-urls
                     (list "https://data.guix-patches.cbaines.net"))
                    (non-derivation-substitute-urls
                     (list "https://builds.guix-patches.cbaines.net"
                           "https://ci.guix.gnu.org"
                           "https://bayfront.guix.gnu.org"
                           "https://guix.cbaines.net")))))
    (modify-services
        %base-services
      (guix-service-type
       config => (guix-configuration
                  (inherit config)
                  (max-silent-time (* 3 60 60))
                  (timeout (* 48 60 60))
                  (extra-options '("--max-jobs=4"))
                  (authorized-keys
                   (cons*
                    (plain-file
                     "guix.cbaines.net.pub"
                     "
(public-key
 (ecc
  (curve Ed25519)
  (q #5F5F4F321533D3A38F909785E682798933BA9BE257C97E5ABC07DD08F27B8DBF#)
  )
 )")
                    (plain-file
                     "bayfront.guix.gnu.org.pub"
                     "
(public-key
 (ecc
  (curve Ed25519)
  (q #7D602902D3A2DBB83F8A0FB98602A754C5493B0B778C8D1DD4E0F41DE14DE34F#)
  )
 )")
                    (plain-file
                     "data.guix-patches.cbaines.net.pub"
                     "
(public-key
 (ecc
  (curve Ed25519)
  (q #50349E83123851A65A569304A12E512B698223A81E10BEEC5A3E56EDAEE5DAC1#)
  )
 )")
                    %default-authorized-guix-keys))))))))
