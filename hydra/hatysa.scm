;; Guix System configuration file for a SolidRun Honeycomb LX2 machine
;; Copyright © 2016, 2017, 2018 Ludovic Courtès <ludo@gnu.org>
;; Copyright © 2021 Christopher Baines <mail@cbaines.net>
;; Released under the GNU GPLv3 or any later version.

(use-modules (guix) (gnu))
(use-service-modules networking mcron ssh monitoring guix web certbot)
(use-package-modules screen ssh linux certs)

(define (sysadmin name full-name)
  (user-account
   (name name)
   (comment full-name)
   (group "users")
   (supplementary-groups '("wheel" "kvm"))
   (home-directory (string-append "/home/" name))))

(define %accounts
  (list (sysadmin "cbaines" "Christopher Baines")))

(define %default-extra-linux-options
  (@@ (gnu packages linux) %default-extra-linux-options))
(define make-linux-libre*
  (@@ (gnu packages linux) make-linux-libre*))

(define-public linux-libre-arm64-honeycomb
  (make-linux-libre* linux-libre-version
                     linux-libre-gnu-revision
                     linux-libre-source
                     '("aarch64-linux")
                     #:extra-version "arm64-generic"
                     #:extra-options
                     (append
                      `(;; needed to fix the RTC on rockchip platforms
                        ("CONFIG_RTC_DRV_RK808" . #t)
                        ;; Pinebook display, battery, charger and usb
                        ("CONFIG_DRM_ANALOGIX_ANX6345" . m)
                        ("CONFIG_CHARGER_AXP20X" . m)
                        ("CONFIG_INPUT_AXP20X_PEK" . m)
                        ("CONFIG_CHARGER_AXP20X" . m)
                        ("CONFIG_BATTERY_AXP20X" . m)
                        ("CONFIG_PINCTRL_AXP209" . m)
                        ("CONFIG_AXP20X_POWER" . m)
                        ("CONFIG_AXP20X_ADC" . m)

                        ("CONFIG_GPIO_SYSFS" . #t)
                        ("CONFIG_GPIO_MPC8XXX" . #t)
                        ("CONFIG_NET_PKTGEN" . #t)
                        ("CONFIG_USB_SERIAL" . #t)
                        ("CONFIG_USB_SERIAL_CONSOLE" . #t)
                        ("CONFIG_USB_SERIAL_GENERIC" . #t)
                        ("CONFIG_USB_SERIAL_SIMPLE" . #t)
                        ("CONFIG_USB_SERIAL_FTDI_SIO" . #t)
                        ("CONFIG_USB_ACM" . #t)
                        ("CONFIG_USB_NET_DRIVERS" . #t)
                        ("CONFIG_USB_USBNET" . #t)
                        ("CONFIG_USB_NET_CDCETHER" . #t)
                        ("CONFIG_USB_NET_CDC_NCM" . #t)
                        ("CONFIG_USB_NET_NET1080" . #t)
                        ("CONFIG_USB_NET_CDC_SUBSET_ENABLE" . #t)
                        ("CONFIG_USB_NET_CDC_SUBSET" . #t)
                        ("CONFIG_USB_ARMLINUX" . #t)
                        ("CONFIG_BLK_DEV_NVME" . #t)
                        ("CONFIG_NVMEM_BCM_OCOTP" . #t)
                        ("CONFIG_DRM_AMDGPU" . #t)
                        ("CONFIG_DRM_AMDGPU_SI" . #t)
                        ("CONFIG_DRM_AMDGPU_CIK" . #t)
                        ("CONFIG_DRM_AMDGPU_USERPTR" . #t)
                        ("CONFIG_DRM_AMD_DC" . #t)
                        ("CONFIG_CHASH" . #t)
                        ("CONFIG_PMBUS" . #t)
                        ("CONFIG_SENSORS_PMBUS" . #t)
                        ("CONFIG_REGULATOR" . #t)
                        ("CONFIG_REGULATOR_FIXED_VOLTAGE" . #t)
                        ("CONFIG_REGULATOR_PWM" . #t)
                        ("CONFIG_SENSORS_AMC6821" . #t)
                        ("CONFIG_SENSORS_LM90" . #t)
                        ("CONFIG_SENSORS_LTC2978" . #t)
                        ("CONFIG_SENSORS_LTC2978_REGULATOR" . #t)
                        ("CONFIG_TMPFS" . #t)
                        ("CONFIG_TMPFS_POSIX_ACL" . #t)
                        ("CONFIG_TMPFS_XATTR" . #t)
                        ("CONFIG_BLK_DEV_RAM_SIZE=5242" . #t)
                        ("CONFIG_POWER_RESET_GPIO" . #t)
                        ("CONFIG_CRYPTO_USER_API_HASH" . #t)
                        ("CONFIG_CRYPTO_USER_API_SKCIPHER" . #t)
                        ("CONFIG_CRYPTO_USER_API_RNG" . #t)
                        ("CONFIG_CRYPTO_USER_API_AEAD" . #t)

                        ;; Pinebook PRO battery and sound support
                        ("CONFIG_BATTERY_CW2015" . m)
                        ("CONFIG_CHARGER_GPIO" . m)
                        ("CONFIG_SND_SOC_ES8316" . m))
                      %default-extra-linux-options)))

(define %nginx-server-blocks
  (let ((common-locations
         (list
          (nginx-location-configuration
           (uri "~ \\.narinfo/info$")
           (body '("proxy_pass http://nar-herder;"
                   "proxy_http_version 1.1;"
                   "proxy_set_header Connection \"\";"
                   "proxy_ignore_client_abort on;")))
          (nginx-location-configuration
           (uri "= /metrics")
           (body '("proxy_pass http://nar-herder;"
                   "proxy_http_version 1.1;"
                   "proxy_set_header Connection \"\";"))))))

    (list
     (nginx-server-configuration
      (server-name '("hatysa.cbaines.net"))
      (listen '("80" "[::]:80"))
      (root "/var/www")
      (locations
       (append
        common-locations
        (list
         (nginx-location-configuration  ; For use by Certbot
          (uri "/.well-known")
          (body '(("root /var/www;"))))))))

     (nginx-server-configuration
      (server-name '("hatysa.cbaines.net"))
      (listen '("443 ssl" "[::]:443 ssl"))
      (root "/var/www")
      (ssl-certificate
       "/etc/letsencrypt/live/hatysa.cbaines.net/fullchain.pem")
      (ssl-certificate-key
       "/etc/letsencrypt/live/hatysa.cbaines.net/privkey.pem")
      (raw-content
       '("
# Make sure SSL is disabled.
ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;

# Disable weak cipher suites.
ssl_ciphers         HIGH:!aNULL:!MD5;
ssl_prefer_server_ciphers on;"))
      (locations common-locations)))))

(define btrfs-balance-job
  #~(job '(next-hour-from (next-day (range 1 31 3)) '(5))
         (lambda ()
           (system* #$(file-append btrfs-progs "/bin/btrfs")
                    "balance" "start" "-dusage=5"
                    "/var/lib/nars"))
         "btrfs-balance"))

(define btrfs-scrub-job
  #~(job '(next-hour-from (next-day (range 1 31 7)) '(5))
         (lambda ()
           (system* #$(file-append btrfs-progs "/bin/btrfs")
                    "scrub" "start" "/var/lib/nars"))
         "btrfs-scrub"))

(operating-system
  (host-name "hatysa")
  (timezone "Europe/London")
  (locale "en_US.utf8")

  (bootloader (bootloader-configuration
               (bootloader grub-efi-bootloader)
               (targets '("/boot/efi"))))

  (kernel linux-libre-arm64-honeycomb)
  (initrd-modules '())
  (kernel-arguments '("arm-smmu.disable_bypass=0"
                      "iommu.passthrough=1"))

  (file-systems (cons* (file-system
                         (device "/dev/nvme0n1p2")
                         (mount-point "/")
                         (type "ext4"))
                       (file-system
                        (device (uuid "f47f0803-0bdb-4152-93b6-c4719c64d459"))
                        (mount-point "/var/lib/nars")
                         (type "btrfs"))
                       (file-system
                         (device "/dev/nvme0n1p1")
                         (mount-point "/boot/efi")
                         (type "vfat"))
                       %base-file-systems))

  (swap-devices (list (swap-space (target "/swapfile"))))

  (users (append %accounts %base-user-accounts))
  (packages
   (cons* screen
          btrfs-progs
          nss-certs
          %base-packages))
  (services
   (cons* (service dhcp-client-service-type)

          (service mcron-service-type
                   (mcron-configuration
                    (jobs
                     (list #~(job '(next-hour '(3)) "guix gc -F 250G")
                           btrfs-balance-job
                           btrfs-scrub-job))))

          (service agetty-service-type
                   (agetty-configuration
                    (tty "ttyAMA0")
                    (keep-baud? #t)
                    (term "vt220")
                    (baud-rate "115200,38400,9600")))

          (service openssh-service-type
                   (openssh-configuration
                    (password-authentication? #f)))

          (service ntp-service-type)
          (service prometheus-node-exporter-service-type)

          (service guix-build-coordinator-agent-service-type
                   (guix-build-coordinator-agent-configuration
                    (coordinator "https://coordinator.bayfront.guix.gnu.org")
                    (authentication
                     (guix-build-coordinator-agent-password-file-auth
                      (uuid "bc5f3585-03f9-4ab3-8604-6acc89512fba")
                      (password-file
                       "/etc/guix-build-coordinator-agent-password")))
                    (max-parallel-builds 8)
                    (systems '("aarch64-linux" "armhf-linux"))
                    (derivation-substitute-urls
                     (list "https://data.guix.gnu.org"
                           "https://data.qa.guix.gnu.org"))
                    (non-derivation-substitute-urls
                     (list "https://bordeaux.guix.gnu.org"))))

          (service nar-herder-service-type
                   (nar-herder-configuration
                    (mirror "https://bordeaux.guix.gnu.org")
                    (storage "/var/lib/nars")))

          (service certbot-service-type
                   (certbot-configuration
                    (certificates
                     (list (certificate-configuration
                            (domains '("hatysa.cbaines.net")))))
                    (email "mail@cbaines.net")
                    (webroot "/var/www")))

          (service nginx-service-type
                   (nginx-configuration
                    (upstream-blocks
                     (list (nginx-upstream-configuration
                            (name "nar-herder")
                            (servers '("127.0.0.1:8734"))
                            (extra-content '("keepalive 2;")))))
                    (server-blocks
                     %nginx-server-blocks)))

          (modify-services %base-services
            (guix-service-type
             config => (guix-configuration
                        (inherit config)
                        (substitute-urls
                         '("https://bordeaux.guix.gnu.org"))

                        (max-silent-time (* 12 3600))
                        (timeout (* 72 3600))

                        (build-accounts 32)

                        (authorized-keys
                         (list
                          (local-file "keys/guix/bordeaux.guix.gnu.org-export.pub")
                          (local-file "keys/guix/data.guix.gnu.org.pub")
                          (local-file "keys/guix/data.qa.guix.gnu.org.pub")))))))))
