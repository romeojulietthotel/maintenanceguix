;;; GNU Guix system administration tools.
;;;
;;; Copyright © 2019-2023 Ludovic Courtès <ludo@gnu.org>
;;; Copyright © 2020, 2021 Ricardo Wurmus <rekado@elephly.net>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (sysadmin web)
  #:use-module (guix git)
  #:use-module (guix gexp)
  #:use-module (guix modules)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:use-module (guix git-download)
  #:use-module (gnu packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages graphviz)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages guile-xyz)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu services)
  #:use-module (gnu services mcron)
  #:use-module (gnu services shepherd)
  #:use-module (gnu services web)
  #:use-module (gnu system shadow)
  #:use-module (sysadmin nginx)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:export (build-program

            static-web-site-configuration
            static-web-site-configuration?

            static-web-site-service-type

            guix-web-site-service-type
            gwl-web-service-type))

(define guix-extensions
  (match (package-transitive-propagated-inputs
          (specification->package "guix"))
    (((labels packages) ...)
     (cons (specification->package "guix") packages))))

(define* (build-program url root
                        #:key
                        (max-duration 3600)
                        (file "guix.scm")
                        (ref '(branch . "master"))
                        (name "build-program")
                        (environment-variables '())
                        (cache-directory #f))
  "Return a program that pulls code from URL, builds it by loading FILE from
that checkout (usually a 'guix.scm' file), and registers the result as
ROOT (an indirect GC root) upon success.  FILE is loaded in a content where
ENVIRONMENT-VARIABLES, a list of key/value pairs, are defined.

The typical use case is polling from the source repository of a web site
that's built with Haunt or similar."
  (define build
    (with-extensions guix-extensions
      #~(begin
          (use-modules (guix) (guix git) (guix ui)
                       (srfi srfi-11) (srfi srfi-19)
                       (ice-9 match))

          (define (root-installed drv root)
            (mbegin %store-monad
              ((store-lift add-indirect-root) root)
              (let ((pivot (string-append root ".tmp")))
                (symlink (derivation->output-path drv) pivot)
                (rename-file pivot root)
                (return #t))))

          (define cache
            (and=> #$cache-directory
                   (lambda (directory)
                     ;; Interpret DIRECTORY as relative to $HOME/.cache.
                     (string-append (%repository-cache-directory)
                                    "/" directory))))

          ;; Properly handle UTF-8 file names in the repo.
          (setenv "GUIX_LOCPATH"
                  #+(file-append glibc-utf8-locales "/lib/locale"))
          (setlocale LC_ALL "en_US.utf8")

          (define-values (checkout commit relation)
            (apply update-cached-checkout #$url #:ref '#$ref
                   (if cache
                       `(#:cache-directory ,cache)
                       '())))

          (define obj
            (let ((variables '#$environment-variables))
              (for-each (match-lambda
                          ((name . value)
                           (setenv name value)))
                        variables)
              (primitive-load (string-append checkout "/" #$file))))

          (define (timestamp)
            (date->string (time-utc->date (current-time time-utc))
                          "[~4]"))

          (define (alarm-handler . _)
            (format #t "~a time is up, aborting web site update~%"
                    (timestamp))
            (exit 1))

          ;; Since this program typically runs periodically, abort after
          ;; MAX-DURATION to avoid spending countless instances of it when,
          ;; for example, the GC lock is held.
          (sigaction SIGALRM alarm-handler)
          (alarm #$max-duration)

          (format #t "~a building web site from '~a'...~%"
                  (timestamp) #$url)

          (with-store store
            (run-with-store store
              (mlet %store-monad ((drv (lower-object obj)))
                (mbegin %store-monad
                  (show-what-to-build* (list drv))
                  (built-derivations (list drv))
                  (root-installed drv #$root))))))))

  (program-file name build
                #:guile guile-3.0-latest))


;;;
;;; Service.
;;;

(define-record-type* <static-web-site-configuration>
  static-web-site-configuration make-static-web-site-configuration
  static-web-site-configuration?
  (git-url     static-web-site-configuration-git-url)
  (git-ref     static-web-site-configuration-git-ref
               (default '(branch . "master")))
  (build-file  static-web-site-configuration-build-file
               (default "guix.scm"))
  (period      static-web-site-configuration-period
               (default 3600))                    ;seconds
  (environment-variables static-web-site-configuration-environment-variable
                         (default '()))
  (cache-directory static-web-site-configuration-cache-directory
                   (default #f))
  (directory   static-web-site-configuration-directory
               (default "/srv/www")))

(define (static-web-site-mcron-jobs sites)
  (define (update config)
    (build-program (static-web-site-configuration-git-url config)
                   (static-web-site-configuration-directory config)
                   #:max-duration (static-web-site-configuration-period config)
                   #:file (static-web-site-configuration-build-file config)
                   #:ref (static-web-site-configuration-git-ref config)
                   #:environment-variables
                   (static-web-site-configuration-environment-variable config)
                   #:cache-directory
                   (static-web-site-configuration-cache-directory config)
                   #:name (string-append
                           "update-"
                           (basename
                            (static-web-site-configuration-directory config)))))

  (define (record->list record)
    (let ((fields (record-type-fields <static-web-site-configuration>)))
      (map (lambda (n)
             (struct-ref record n))
           (iota (length fields)))))

  (map (lambda (config)
         ;; Add an offset to spread web site updates over the period to avoid I/O
         ;; load peaks when there are several such jobs.  Compute a hash over
         ;; a list representation of CONFIG, rather than over CONFIG, because
         ;; hash of a struct depends on the object identity of its vtable.
         (let* ((period (static-web-site-configuration-period config))
                (offset (hash (record->list config) period)))
           #~(job (lambda (now)
                    (let ((elapsed (modulo now #$period)))
                      (+ now (- #$period elapsed) #$offset)))
                  #$(update config)
                  #:user "static-web-site")))
       sites))

(define (static-web-site-activation sites)
  (with-imported-modules '((guix build utils))
    #~(begin
        (use-modules (guix build utils))

        (for-each (lambda (directory)
                    (let ((directory (dirname directory)))
                      (mkdir-p directory)
                      (chown directory
                             (passwd:uid (getpw "static-web-site"))
                             (group:gid (getgr "static-web-site")))))
                  '#$(map static-web-site-configuration-directory
                          sites)))))

(define static-web-site-accounts
  (const (list (user-account
                (name "static-web-site")
                (group "static-web-site")
                (system? #t))
               (user-group
                (name "static-web-site")
                (system? #t)))))

(define static-web-site-service-type
  ;; Services of this type take a list of <static-web-site-configuration>
  ;; records and can be extended with additional records.
  (service-type (name 'static-web-site)
                (compose concatenate)
                (extend append)
                (extensions
                 (list (service-extension mcron-service-type
                                          static-web-site-mcron-jobs)
                       (service-extension account-service-type
                                          static-web-site-accounts)
                       (service-extension activation-service-type
                                          static-web-site-activation)))
                (description
                 "Update and publish a web site that is built from source
taken from a Git repository.")
                (default-value '())))


;;;
;;; GNU Guix web site at guix.gnu.org.
;;;

(define guix-static-web-sites
  ;; TODO: Add the manual of Cuirass.
  (list (static-web-site-configuration
         (git-url
          "https://git.savannah.gnu.org/git/guix/guix-artwork.git")
         (directory "/srv/guix.gnu.org")
         (build-file "website/.guix.scm"))

        ;; Manual for the latest stable release.
        (static-web-site-configuration
         (git-url "https://git.savannah.gnu.org/git/guix.git")
         (git-ref '(branch . "version-1.4.0"))
         (period (* 24 3600))                     ;check once per day
         (directory "/srv/guix-manual")
         (build-file "doc/build.scm")
         (environment-variables '(("GUIX_MANUAL_VERSION" . "1.4.0")
                                  ("GUIX_WEB_SITE_URL" . "/"))))

        ;; Manual for 'master'.
        (static-web-site-configuration
         (git-url "https://git.savannah.gnu.org/git/guix.git")
         (directory "/srv/guix-manual-devel")

         ;; XXX: Use a different cache directory to work around the fact that
         ;; (guix git) would use a same-named checkout directory for 'master'
         ;; and for the branch above.  Since both mcron jobs run at the same
         ;; time, they would end up using one branch or the other, in a
         ;; non-deterministic way.
         (cache-directory "guix-master-manual")

         (build-file "doc/build.scm")
         (environment-variables '(("GUIX_WEB_SITE_URL" . "/"))))

        ;; Cookbook for 'master'.
        (static-web-site-configuration
         (git-url "https://git.savannah.gnu.org/git/guix.git")
         (directory "/srv/guix-cookbook")

         ;; XXX: Use a different cache directory (see above).
         (cache-directory "guix-cookbook-master")

         (build-file "doc/build.scm")
         (environment-variables '(("GUIX_MANUAL" . "guix-cookbook")
                                  ("GUIX_WEB_SITE_URL" . "/"))))))

(define %package-metadata-directory
  ;; Directory where to store 'packages.json' and 'sources.json'.
  "/srv/package-metadata")

(define guix-web-site-mcron-jobs
  ;; Job that periodically builds 'packages.json' and 'sources.json'.
  (let ((program (program-file "build-package-metadata"
                               #~(execl #$(file-append guix "/bin/guix")
                                        "guix"
                                        "time-machine" "--" "repl" "--"
                                        #$(local-file
                                           "../../build-package-metadata.scm")
                                        #$%package-metadata-directory))))
    (list #~(job "30 */6 * * *" #$program
                 #:user "static-web-site"))))

(define guix-web-site-activation
  (with-imported-modules '((guix build utils))
    #~(begin
        (use-modules (guix build utils))

        (let ((pw (getpwnam "static-web-site")))
          (mkdir-p #$%package-metadata-directory)
          (chown #$%package-metadata-directory
                 (passwd:uid pw) (passwd:gid pw))
          (chmod #$%package-metadata-directory #o755)))))

(define guix-web-site-service-type
  (service-type
   (name 'guix-web-site)
   (extensions
    (list (service-extension static-web-site-service-type
                             (const guix-static-web-sites))
          (service-extension activation-service-type
                             (const guix-web-site-activation))
          (service-extension mcron-service-type
                             (const guix-web-site-mcron-jobs))
          (service-extension nginx-service-type
                             (const (list guix.gnu.org-nginx-server)))))
   (description
    "This service provides the web site of the GNU Guix project.")
   (default-value #t)))


;;;
;;; Guix Workflow Language.
;;;

(define (gwl-web-shepherd-service gwl)
  (define wrapped-guix
    (program-file "guix-workflow"
                  #~(begin
                      (setenv "GUIX_EXTENSIONS_PATH"
                              (string-append '#$gwl
                                             (string-append "/share/guix/extensions")))
                      (apply execl
                             #$(file-append guix "/bin/guix")
                             (command-line)))))

  (with-imported-modules (source-module-closure
                          '((gnu build shepherd)
                            (gnu system file-systems)))
    (list (shepherd-service
           (provision '(gwl-web))
           (requirement '(user-processes networking))
           (documentation "Run Guix Workflow Language web server.")
           (modules '((gnu build shepherd)
                      (gnu system file-systems)))
           (start #~(make-forkexec-constructor/container
                     (list #$wrapped-guix "workflow" "web")
                     #:user "gwl-web"
                     #:group "gwl-web"
                     #:log-file "/var/log/gwl.log"))
           (stop #~(make-kill-destructor))))))

(define %gwl-web-accounts
  (list (user-account
         (name "gwl-web")
         (system? #t)
         (home-directory "/var/empty")
         (group "gwl-web"))
        (user-group
         (name "gwl-web")
         (system? #t))))

(define gwl-web-service-type
  (service-type
   (name 'gwl-web)
   (extensions
    (list (service-extension shepherd-root-service-type
                             gwl-web-shepherd-service)
          (service-extension account-service-type
                             (const %gwl-web-accounts))))
   (default-value gwl)
   (description
    "Run @command{guix worflow web}, which serves the Guix Workflow
Language (GWL) web site.")))

