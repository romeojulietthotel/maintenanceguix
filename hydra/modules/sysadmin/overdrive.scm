;;; SoftIron OverDrive 1000 build machines.
;;;
;;; Copyright © 2016-2018, 2020-2022 Ludovic Courtès <ludo@gnu.org>
;;; Copyright © 2020, 2021 Mathieu Othacehe <othacehe@gnu.org>
;;; Copyright © 2022, 2023 Andreas Enge <andreas@enge.fr>
;;; Copyright © 2022 Maxim Cournoyer <maxim.cournoyer@gmail.com>
;;; Copyright © 2024 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (sysadmin overdrive)
  #:use-module (gnu)
  #:use-module (gnu packages screen)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages certs)
  #:use-module (gnu services avahi)
  #:use-module (gnu services cuirass)
  #:use-module (gnu services linux)
  #:use-module (gnu services networking)
  #:use-module (gnu services mcron)
  #:use-module (gnu services ssh)
  #:use-module (gnu services vpn)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module ((sysadmin services) #:select (berlin-wireguard-peer))
  #:export (overdrive-system))

(define (sysadmin name full-name)
  (user-account
   (name name)
   (comment full-name)
   (group "users")
   (supplementary-groups '("wheel" "kvm"))
   (home-directory (string-append "/home/" name))))

(define %accounts
  (list (sysadmin "ludo" "Ludovic Courtès")
        (sysadmin "rekado" "Ricardo Wurmus")
        (sysadmin "mathieu" "Mathieu Othacehe")
        (sysadmin "maxim" "Maxim Cournoyer")
        (sysadmin "cbaines" "Christopher Baines")
        (sysadmin "roptat" "Julien Lepiller")
        (sysadmin "janneke" "Janneke Nieuwenhuizen")
        (user-account
         (name "hydra")
         (comment "Hydra User")
         (group "users")
         (home-directory (string-append "/home/" name)))))

(define %authorized-guix-keys
  ;; List of authorized 'guix archive' keys.
  (list (local-file "../../keys/guix/berlin.guixsd.org-export.pub")

        ;; Exceptionally, allow exports from Ludo's laptop.  This is
        ;; necessary for the release process.
        (local-file "../../keys/guix/ludo-laptop-export.pub")
        ;; Likewise for Maxim's desktop.
        (local-file "../../keys/guix/maxim-desktop-export.pub")))

(define gc-job
  ;; Run 'guix gc' at 3AM every day.
  #~(job '(next-hour '(3)) "guix gc -F 50G"))

(define btrfs-balance-job
  ;; Re-allocate chunks which are using less than 5% of their chunk
  ;; space, to regain Btrfs 'unallocated' space.  The usage is kept
  ;; low (5%) to minimize wear on the SSD.  Runs at 5 AM every 3 days.
  #~(job '(next-hour-from (next-day (range 1 31 3)) '(5))
         (lambda ()
           (system* #$(file-append btrfs-progs "/bin/btrfs")
                    "balance" "start" "-dusage=5" "/"))
         "btrfs-balance"))

(define %common-btrfs-options '(("compress-force" . "zstd")
                                ("space_cache" . "v2")))

(define* (overdrive-system name #:key wireguard-ip)
  (operating-system
    (host-name name)
    (timezone "Europe/Paris")
    (locale "en_US.UTF-8")

    (bootloader (bootloader-configuration
                 (bootloader grub-efi-bootloader)
                 (targets (list "/boot/efi"))))
    (initrd-modules (cons* "xhci-pci" "ahci_platform" "sg" "sd_mod" "ahci_dwc"
                           %base-initrd-modules))
    (firmware '())
    (file-systems (cons* (file-system
                           (device "/dev/sda3")
                           (mount-point "/")
                           (type "btrfs")
                           (options (alist->file-system-options
                                     %common-btrfs-options)))
                         (file-system
                           (device "/dev/sda1")
                           (mount-point "/boot/efi")
                           ;; original options:
                           ;; (rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,errors=remount-ro)
                           (type "vfat"))
                         %base-file-systems))

    (swap-devices (list (swap-space (target "/dev/sda4"))))

    (users (append %accounts %base-user-accounts))
    (services
     (cons*
      (service openssh-service-type
               (openssh-configuration
                (password-authentication? #f)
                (authorized-keys
                 `(("ludo" ,(local-file "../../keys/ssh/ludo.pub"))
                   ("rekado" ,(local-file "../../keys/ssh/rekado.pub"))
                   ("dannym" ,(local-file "../../keys/ssh/dannym.pub"))
                   ("janneke" ,(local-file "../../keys/ssh/janneke.pub"))
                   ("mathieu" ,(local-file "../../keys/ssh/mathieu.pub"))
                   ("maxim" ,(local-file "../../keys/ssh/maxim.pub"))
                   ("andreas" ,(local-file "../../keys/ssh/andreas.pub"))))))

      (service avahi-service-type)
      (service dhcp-client-service-type)
      (service mcron-service-type
               (mcron-configuration
                (jobs (list gc-job btrfs-balance-job))))

      (service agetty-service-type
               (agetty-configuration
                (tty "ttyAMA0")
                (keep-baud? #t)
                (term "vt220")
                (baud-rate "115200,38400,9600")))
      (service cuirass-remote-worker-service-type
               (cuirass-remote-worker-configuration
                (workers 2)
                (server "10.0.0.1:5555") ;berlin
                (systems '("armhf-linux" "aarch64-linux"))
                (substitute-urls '("http://10.0.0.1"))))
      (service wireguard-service-type
               (wireguard-configuration
                (addresses (list wireguard-ip))
                (peers
                 (list berlin-wireguard-peer))))
      (service zram-device-service-type (zram-device-configuration
                                         (size "4G")
                                         (compression-algorithm 'ZSTD)
                                         (priority 100)))

      (service ntp-service-type)

      (modify-services %base-services
        (guix-service-type config =>
                           (guix-configuration
                            (inherit config)
                            (max-silent-time 7200)

                            ;; The overdrives are part of the ci.guix build
                            ;; farm and take substitutes from there
                            ;; exclusively.
                            (substitute-urls
                             '("https://ci.guix.gnu.org"))
                            (authorized-keys
                             %authorized-guix-keys)
                            (extra-options
                             '("--max-jobs=2" "--cores=3")))))))

    (packages (cons* btrfs-progs screen openssh strace nss-certs
                     %base-packages))

    ;; Allow sysadmins (sudoers) to use 'sudo' without a password so
    ;; they can 'guix deploy' these machines as their own user.
    (sudoers-file
     (plain-file "sudoers"
                 (string-join
                  (append (remove (cut string-prefix? "%wheel" <>)
                                  (string-split
                                   (string-trim-right (plain-file-content
                                                       %sudoers-specification))
                                   #\newline))
		          (list "%wheel ALL = NOPASSWD: ALL\n")) "\n")))))
