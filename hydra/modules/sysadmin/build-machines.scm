;;; GNU Guix system administration tools.
;;;
;;; Copyright © 2016-2017, 2019-2023 Ludovic Courtès <ludo@gnu.org>
;;; Copyright © 2017, 2018, 2019 Ricardo Wurmus <rekado@elephly.net>
;;; Copyright © 2020 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (sysadmin build-machines)
  #:use-module (gnu)
  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader grub)
  #:use-module (gnu services avahi)
  #:use-module (gnu services base)
  #:use-module (gnu services cuirass)
  #:use-module (gnu services ssh)
  #:use-module (gnu services mcron)
  #:use-module (gnu services monitoring)
  #:use-module (gnu services networking)
  #:use-module (gnu services virtualization)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (sysadmin people)
  #:use-module (gnu packages ssh)
  #:use-module (ice-9 format)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:export (build-machine-os
            berlin-new-build-machine-os
            childhurd-ip?))

;;; Commentary:
;;;
;;; Configuration of build machines.
;;;
;;; Code:

(define* (build-machine-os host-name sysadmins
                           #:key (authorized-guix-keys '()))
  "Return the <operating-system> declaration for a build machine called
HOST-NAME and accessibly by SYSADMINS, with the given AUTHORIZED-GUIX-KEYS."
  (define gc-job
    ;; Run 'guix gc' at 3AM every day.
    #~(job '(next-hour '(3))
           "guix gc -F 40G"))

  (operating-system
    (host-name host-name)
    (timezone "Europe/Paris")
    (locale "en_US.UTF-8")

    (name-service-switch %mdns-host-lookup-nss)
    (bootloader (bootloader-configuration
                 (bootloader grub-bootloader)
                 (target "/dev/sda")))
    (file-systems (cons* (file-system
                           (device (file-system-label "my-root"))
                           (mount-point "/")
                           (type "ext4"))
                         (file-system
                           (device "tmpfs")
                           (mount-point "/var/guix/temproots")
                           (type "tmpfs")
                           (flags '(no-suid no-dev no-exec))
                           (check? #f))
                         %base-file-systems))

    (services (cons* (service sysadmin-service-type sysadmins)
                     (service openssh-service-type)
                     (service dhcp-client-service-type)
                     (service avahi-service-type
                              (avahi-configuration (debug? #t)))
                     (service mcron-service-type (list gc-job))
                     (modify-services %base-services
                       (guix-service-type config =>
                                          (guix-configuration
                                           (inherit config)
                                           (authorized-keys
                                            authorized-guix-keys))))))))

(define (childhurd-ip? ip)
  "Return #t if IP should be running a Childhurd."
  (member ip '("141.80.167.158" "141.80.167.159"
               "141.80.167.160" "141.80.167.161")))

(define* (berlin-new-build-machine-os id
                                      #:key
                                      (authorized-guix-keys '())
                                      (emulated-architectures '())
                                      (systems
                                       '("x86_64-linux" "i686-linux"))
                                      childhurd?
                                      (max-jobs 4)
                                      (max-cores 16)
                                      (build-accounts-to-max-jobs-ratio 4))
  "Return the <operating-system> declaration for a build machine for
berlin.guixsd.org with integer ID, with the given AUTHORIZED-GUIX-KEYS.  Add a
'qemu-binfmt-service' for EMULATED-ARCHITECTURES, unless it's empty.  Start a
Cuirass remote worker building substitutes for the given SYSTEMS."

  (define gc-job
    ;; Run 'guix gc' at 3AM and 3PM every day.
    #~(job '(next-hour '(3 15))
           "guix gc -F 150G"))

  (define childhurd-gc-job
    ;; Run 'guix gc' at 2AM and 2PM every day.
    #~(job '(next-hour '(2 14))
           "guix gc -F 2G"))

  (define childhurd-os
    (operating-system
      (inherit %hurd-vm-operating-system)
      (host-name (format #f "berlin-childhurd-~3,'0d" id))
      (hosts-file
       (plain-file "hosts"
                   (string-append "127.0.0.1 localhost " host-name "\n"
                                  "::1       localhost " host-name "\n"
                                  "141.80.167.131 ci.guix.gnu.org\n")))
      (services
       (cons* (service mcron-service-type
                       (mcron-configuration
                        (jobs (list childhurd-gc-job))))
              (modify-services
               (operating-system-user-services %hurd-vm-operating-system)
               (guix-service-type
                config =>
                (guix-configuration
                 (inherit config)
                 (use-substitutes? #f)))
               (openssh-service-type
                 config =>
                 (openssh-configuration
                  (inherit config)
                  (authorized-keys
                   `(("hydra"
                      ,(local-file "../../keys/ssh/berlin.guixsd.org.pub")))))))))))

  (define (childhurd-net-options config)
    "Expose SSH and VNC ports on 0.0.0.0; for first Childhurd VM those
are 10022 and 15900.  Keep secret-service port local."
    `("--device" "rtl8139,netdev=net0"
      "--netdev" ,(string-append
                   "user,id=net0"
                   ",hostfwd=tcp:127.0.0.1:"
                   (number->string (hurd-vm-port
                                    config
                                    (@@ (gnu services virtualization)
                                        %hurd-vm-secrets-port)))
                   "-:1004"
                   ",hostfwd=tcp:127.0.0.1:"
                   (number->string (hurd-vm-port
                                    config
                                    (@@ (gnu services virtualization)
                                        %hurd-vm-ssh-port)))
                   "-:22"
                   ",hostfwd=tcp:127.0.0.1:"
                   (number->string (hurd-vm-port
                                    config
                                    (@@ (gnu services virtualization)
                                        %hurd-vm-vnc-port)))
                   "-:5900")))

  (define sysadmins
    (list (sysadmin (name "ludo")
                    (full-name "Ludovic Courtès")
                    (ssh-public-key (local-file "../../keys/ssh/ludo.pub")))
          (sysadmin (name "rekado")
                    (full-name "Ricardo Wurmus")
                    (ssh-public-key (local-file "../../keys/ssh/rekado.pub")))
          (sysadmin (name "mathieu")
                    (full-name "Mathieu Othacehe")
                    (ssh-public-key
                     (local-file "../../keys/ssh/mathieu.pub")))
          (sysadmin (name "maxim")
                    (full-name "Maxim Cournoyer")
                    (ssh-public-key (local-file "../../keys/ssh/maxim.pub")))
          (sysadmin (name "nckx")
                    (full-name "Tobias Geerinckx-Rice")
                    (ssh-public-key (local-file "../../keys/ssh/nckx.pub")))
          (sysadmin (name "cbaines")
                    (full-name "Christopher Baines")
                    (ssh-public-key (local-file "../../keys/ssh/cbaines.pub")))
          (sysadmin (name "hydra")      ;fake sysadmin
                    (full-name "Hydra User")
                    (restricted? #t)
                    (ssh-public-key
                     (local-file "../../keys/ssh/berlin.guixsd.org.pub")))))
  (operating-system
    (host-name (format #f "hydra-guix-~3,'0d" id))
    (timezone "Europe/Berlin")
    (locale "en_US.utf8")
    (name-service-switch %mdns-host-lookup-nss)
    (kernel-arguments '("console=tty0" "console=ttyS0,57600n8"))
    (keyboard-layout
     (keyboard-layout "us" "altgr-intl"))
    (bootloader
     (bootloader-configuration
      (bootloader grub-efi-bootloader)
      (targets (list "/boot/efi"))
      (keyboard-layout keyboard-layout)
      (terminal-inputs '(serial))
      (terminal-outputs '(serial))))
    (initrd-modules (append (list "megaraid_sas")
                            %base-initrd-modules))
    (hosts-file
     (plain-file "hosts"
                 (string-append "127.0.0.1 localhost " host-name "\n"
                                "::1       localhost " host-name "\n"
                                "141.80.167.131 ci.guix.gnu.org\n")))
    (swap-devices (list (swap-space
                         (target "/dev/sda2"))))
    (file-systems
     (cons* (file-system
              (mount-point "/boot/efi")
              (device "/dev/sda1")
              (type "vfat"))
            (file-system
              (mount-point "/")
              (device (file-system-label "my-root"))
              (type "ext4"))
            %base-file-systems))
    (packages (cons* openssh %base-packages))
    (services
     (cons* (simple-service 'guile-load-path-in-global-env
                            session-environment-service-type
                            `(("GUILE_LOAD_PATH"
                               . "/run/current-system/profile/share/guile/site/3.0")
                              ("GUILE_LOAD_COMPILED_PATH"
                               . ,(string-append "/run/current-system/profile/lib/guile/3.0/site-ccache:"
                                                 "/run/current-system/profile/share/guile/site/3.0"))))
            (service agetty-service-type
                     (agetty-configuration
                      (tty "ttyS0")
                      (baud-rate "57600")))
            (service dhcp-client-service-type)
            (service zabbix-agent-service-type
                     (zabbix-agent-configuration
                      (server '("141.80.167.131"))
                      (server-active '("141.80.167.131"))))
            (service avahi-service-type
                     (avahi-configuration (debug? #t)))
            (service cuirass-remote-worker-service-type
                     (cuirass-remote-worker-configuration
                      (workers max-jobs)
                      (systems systems)
                      (substitute-urls '("http://141.80.167.131"))))
            (service mcron-service-type
                     (mcron-configuration
                      (jobs (list gc-job))))
            (service ntp-service-type
                     (ntp-configuration
                      (allow-large-adjustment? #t)))
            (service sysadmin-service-type sysadmins)
            (service openssh-service-type
                     (openssh-configuration
                      (extra-content "\
Match Address 141.80.167.131
  PermitRootLogin yes")))

            `(,@(if (null? emulated-architectures)
                    '()
                    (list (service qemu-binfmt-service-type
                                   (qemu-binfmt-configuration
                                    (platforms
                                     (apply lookup-qemu-platforms
                                            emulated-architectures))))))

              ,@(if (not childhurd?)
                    '()
                    (list (service hurd-vm-service-type
                                   (hurd-vm-configuration
                                    (os childhurd-os)
                                    ;; 6G should be enough to build 'hello'
                                    (disk-size (* 12000 (expt 2 20))) ;12G
                                    (memory-size (* 4 1024)) ;4GiB
                                    (net-options
                                     (childhurd-net-options this-record))))))

              ,@(modify-services %base-services
                  (guix-service-type
                   config =>
                   (guix-configuration
                    (inherit config)
                    (authorized-keys
                     authorized-guix-keys)

                    ;; Enable substitution requests debug traces to
                    ;; investigate: https://issues.guix.gnu.org/48468.
                    (environment
                     '("GUIX_SUBSTITUTE_DEBUG=1"))

                    ;; Fetch only from berlin.guix.gnu.org, using its local
                    ;; IP address.
                    (substitute-urls '("http://141.80.167.131"))

                    (max-silent-time 3600)
                    (timeout (* 6 3600))
                    (build-accounts
                     (* build-accounts-to-max-jobs-ratio max-jobs))
                    (extra-options
                     (list "--max-jobs"
                           (number->string max-jobs)
                           "--cores"
                           (number->string max-cores)))))))))
    ;; Allow sysadmins (sudoers) to use 'sudo' without a password so
    ;; they can 'guix deploy' these machines as their own user.
    (sudoers-file
     (plain-file "sudoers"
                 (string-join
                  (append (remove (cut string-prefix? "%wheel" <>)
                                  (string-split
                                   (string-trim-right (plain-file-content
                                                       %sudoers-specification))
                                   #\newline))
		          (list "%wheel ALL = NOPASSWD: ALL\n")) "\n")))))

;;; build-machines.scm end here
