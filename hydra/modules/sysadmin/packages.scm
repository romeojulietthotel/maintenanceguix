;;; Packages for GNU Guix project systems.
;;;
;;; Copyright © 2023 Christopher Baines <mail@cbaines.net>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (sysadmin packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages guile-xyz)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages web)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26))

(define-public qa-frontpage
  (let ((commit "d7b2634efa8f953b926eb251cfc26b0225514d87")
        (revision "16"))
    (package
      (name "guix-qa-frontpage")
      (version (git-version "0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://git.savannah.gnu.org/git/guix/qa-frontpage.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "0kr0by5ppbk6n25d2hwsy7zi2f85yzkv391kgk99mdd0yss1jbv1"))))
      (build-system gnu-build-system)
      (arguments
       (list
        #:modules `(((guix build guile-build-system)
                     #:select (target-guile-effective-version))
                    ,@%gnu-build-system-modules)
        #:imported-modules `((guix build guile-build-system)
                             ,@%gnu-build-system-modules)
        #:phases
        #~(modify-phases %standard-phases
            (add-after 'install 'wrap-executable
              (lambda* (#:key inputs outputs target #:allow-other-keys)
                (let* ((out (assoc-ref outputs "out"))
                       (bin (string-append out "/bin"))
                       (guile (assoc-ref inputs "guile"))
                       (version (target-guile-effective-version))
                       (scm (string-append out "/share/guile/site/" version))
                       (go  (string-append out "/lib/guile/" version "/site-ccache")))
                  (for-each
                   (lambda (file)
                     (simple-format (current-error-port) "wrapping: ~A\n" file)
                     (wrap-program file
                       `("PATH" ":" prefix
                         (,(string-append (assoc-ref inputs "git")
                                          "/bin")
                          ,(string-append (assoc-ref inputs "openssh")
                                          "/bin")))
                       `("GUILE_LOAD_PATH" ":" prefix
                         (,scm ,(getenv "GUILE_LOAD_PATH")))
                       `("GUILE_LOAD_COMPILED_PATH" ":" prefix
                         (,go ,(getenv "GUILE_LOAD_COMPILED_PATH")))))
                   (find-files bin))))))))
      (inputs
       (list guix
             guix-data-service
             guile-json-4
             guile-fibers
             guile-kolam
             guile-git
             guile-debbugs
             guile-readline
             guile-prometheus
             guix-build-coordinator
             guile-next
             git
             openssh))
      (native-inputs
       (list autoconf
             automake
             pkg-config
             emacs-minimal
             emacs-htmlize))
      (synopsis "QA Frontpage for Guix")
      (description
       "This service assists with quality assurance within Guix.  Currently
that means assisting with testing patches, but in the intended scope
is any and all quality assurance tasks.")
      (home-page "https://git.cbaines.net/guix/qa-frontpage")
      (license license:agpl3+))))

(define-public guix-cran-scripts
  (let ((commit "077d9e0a3753360b1b1fbd17c356967ba6d92175")
        (revision "8"))
  (package
    (name "guix-cran-scripts")
    (version (git-version "0" revision commit))
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/guix-science/guix-cran-scripts.git")
             (commit commit)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1k2p6gfdl37spy7j3vz47by9yrdh96s9ppin4dq0ymbixsnrq7gg"))))
    (build-system trivial-build-system)
    (arguments
     (list
      #:modules '((guix build utils))
      #:builder
      #~(begin
          (use-modules (guix build utils))
          (let* ((source (assoc-ref %build-inputs "source"))
                 (bin (string-append #$output "/bin"))
                 (import-scm (string-append bin "/import.scm"))
                 (update-sh (string-append bin "/update.sh"))
                 (shell #$(file-append bash-minimal "/bin/bash")))
            (mkdir-p bin)
            (copy-file (string-append source "/import.scm") import-scm)
            (copy-file (string-append source "/update.sh") update-sh)
            ;; No interpreter, cannot use wrap-script.
            ;; Don't wrap, because it has to be called with `guix repl`
            #;
            (wrap-program import-scm
              #:sh shell
              `("PATH" ":" prefix (,#$(file-append git "/bin"))))
            (substitute* update-sh
              (("import.scm") import-scm))
            (chmod update-sh #o555)
            (wrap-program update-sh
              #:sh shell
              `("PATH" ":" prefix (,#$(file-append coreutils "/bin")
                                   ,#$(file-append git "/bin"))))))))
    (home-page "https://github.com/guix-science/guix-cran-scripts")
    (synopsis "Automated CRAN to Guix import")
    (description "Script, which automates the import of all packages from
CRAN into a separate Guix channel.")
    (license license:gpl3+))))
