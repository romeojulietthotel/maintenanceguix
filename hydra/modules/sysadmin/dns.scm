;;; GNU Guix system administration tools.
;;;
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;; Copyright © 2020 Tobias Geerinckx-Rice <me@tobias.gr>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (sysadmin dns)
  #:use-module (gnu services dns)
  #:export (guix.gnu.org-zone
            berlin-ip4
            bayfront-ip4))

;;; Commentary:
;;;
;;; DNS configuration.
;;;
;;; For any change in the zone (an IP, a new record, ...), increment the
;;; the serial in the zone configuration. This is very important for
;;; changes to be taken into account.
;;;
;;; Remember some DNS rules: no other kind of record for a CNAME record.
;;; Always associate a name that resolves to an A or an AAAA record
;;; immediately (it cannot be a CNAME).  Same for MX.
;;;
;;; Remember that data is relative to the root of this zone when it
;;; reference another domain name, unless it ends with a dot.
;;;
;;; Ex: "ns1.guix.gnu.org" actually means "ns1.guix.gnu.org.guix.gnu.org"
;;; whereas "ns1.guix.gnu.org." means what it says.
;;;
;;; Code:

;; Define some IP addresses for easier use later
(define gnu.org-ip4 "209.51.188.148")
(define gnu.org-ip6 "2001:470:142:3::a")
(define hydra-ip4 "18.4.89.46")
(define bayfront-ip4 "185.233.100.56")
(define bayfront-ip6 "2a0c:e300::58")
(define lakeside-ip4 "176.9.98.253")
(define lakeside-ip6 "2a01:4f8:151:40cc::")
(define bishan-ip4 "5.9.22.61")
(define bishan-ip6 "2a01:4f8:161:123::")
(define berlin-ip4 "141.80.181.40")
(define hydra-guix-129-ip4 "141.80.181.41")
(define milano-guix-1-ip4 "159.149.133.203")
(define fosshost-1-ip4 "147.75.35.153")
(define fosshost-2-ip4 "147.75.35.155")
(define overdrive1-ip4 "91.160.117.201")
(define overdrive1-ip6 "2a01:e0a:1d:7270:af76:b9b:ca24:c465")
(define data.qa-ip4 "88.99.33.53")
(define monokuma-ip6 "2a02:8010:68c1:0:e2ff:f7ff:fe00:2059")
(define dover-ip6 "2a02:8010:68c1::e2ff:f7ff:fe00:20b3")
(define hatysa-ip6 "2a02:8010:68c1::d263:b4ff:fe03:b9af")
(define hamal-ip6 "2a02:8010:68c1::d263:b4ff:fe02:e65b")

;; The SOA MNAME and one NS record must always be consistent.
(define primary-ns "ns1.gnu.org.")

(define-zone-entries guix.gnu.org.zone
;; Name       TTL Class Type Data
  ("@"        ""  "IN"  "A"      bayfront-ip4)
  ("@"        ""  "IN"  "AAAA"   bayfront-ip6)
  ("@"        ""  "IN"  "NS"     primary-ns)
  ("@"        ""  "IN"  "NS"     "ns2.gnu.org.")
  ("@"        ""  "IN"  "NS"     "ns3.gnu.org.")
  ("@"        ""  "IN"  "NS"     "ns4.gnu.org.")
  ("ns1"      ""  "IN"  "A"      bayfront-ip4)
  ("ns2"      ""  "IN"  "A"      berlin-ip4)

  ;; CAA records.  See:
  ;; https://tools.ietf.org/html/rfc6844
  ;; https://letsencrypt.org/docs/caa/
  ;; https://docs.aws.amazon.com/acm/latest/userguide/setup-caa.html
  ;;
  ;; The gnu.org domain defines a CAA record which only includes
  ;; letsencrypt.  To allow Amazon ACM to create a TLS certificate for
  ;; ci.guix.gnu.org, we need to override this for our zone.
  ("@"        ""  "IN"  "CAA"    "0 issue \"letsencrypt.org\"")
  ;; The ACM docs suggest any one of these records may be sufficient,
  ;; but to avoid any possibility of problems, let's include all four.
  ("@"        ""  "IN"  "CAA"    "0 issue \"amazon.com\"")
  ("@"        ""  "IN"  "CAA"    "0 issue \"amazontrust.com\"")
  ("@"        ""  "IN"  "CAA"    "0 issue \"awstrust.com\"")
  ("@"        ""  "IN"  "CAA"    "0 issue \"amazonaws.com\"")
  ;; We might want to create wildcard certificates in the future.
  ("@"        ""  "IN"  "CAA"    "0 issuewild \"letsencrypt.org\"")
  ("@"        ""  "IN"  "CAA"    "0 issuewild \"amazon.com\"")
  ("@"        ""  "IN"  "CAA"    "0 issuewild \"amazontrust.com\"")
  ("@"        ""  "IN"  "CAA"    "0 issuewild \"awstrust.com\"")
  ("@"        ""  "IN"  "CAA"    "0 issuewild \"amazonaws.com\"")

  ;; Machines.
  ("hydra"    ""  "IN"  "A"      hydra-ip4)
  ("berlin"   ""  "IN"  "A"      berlin-ip4)
  ("hydra-guix-129" "" "IN" "A"  hydra-guix-129-ip4)
  ("bayfront" ""  "IN"  "A"      bayfront-ip4)
  ("bayfront" ""  "IN"  "AAAA"   bayfront-ip6)
  ("bishan"   ""  "IN"  "A"      bishan-ip4)
  ("bishan"   ""  "IN"  "AAAA"   bishan-ip6)
  ("overdrive1" ""  "IN"  "A"    overdrive1-ip4)
  ("dover"    ""  "IN"  "AAAA"   dover-ip6)
  ("monokuma" ""  "IN"  "AAAA"   monokuma-ip6)
  ("hatysa"   ""  "IN"  "AAAA"   hatysa-ip6)
  ("hamal"    ""  "IN"  "AAAA"   hamal-ip6)

  ;; Services.
  ("issues"   ""  "IN"  "A"      berlin-ip4)
  ("monitor"  ""  "IN"  "A"      berlin-ip4)
  ("dump"     ""  "IN"  "A"      berlin-ip4)
  ("logs"     ""  "IN"  "A"      bayfront-ip4)
  ("ci"       ""  "IN"  "A"      berlin-ip4)
  ("disarchive" ""  "IN"  "A"    berlin-ip4)
  ("data"     ""  "IN"  "A"      "78.47.68.4")
  ("data.qa"  ""  "IN"  "A"      data.qa-ip4)
  ("coordinator.bayfront"
              ""  "IN"  "A"      bayfront-ip4)
  ("coordinator.bayfront"
              ""  "IN"  "AAAA"   bayfront-ip6)
  ("bordeaux" ""  "IN"  "A"      bayfront-ip4)
  ("bordeaux" ""  "IN"  "AAAA"   bayfront-ip6)
  ("10years"  ""  "IN"  "A"      bayfront-ip4)
  ("10years"  ""  "IN"  "AAAA"   bayfront-ip6)
  ("qa"       ""  "IN"  "A"      bayfront-ip4)
  ("qa"       ""  "IN"  "AAAA"   bayfront-ip6)
  ("packages" ""  "IN"  "A"      bayfront-ip4)
  ("packages" ""  "IN"  "AAAA"   bayfront-ip6)

  ;; This record is required in order to prove to Amazon ACM that we
  ;; own the domain.  As long as it exists, ACM will automatically
  ;; renew the TLS certificate for the CloudFront distribution we use
  ;; as the CDN for ci.guix.gnu.org.  See cdn/README.org for details.
  ("_82c0b5947777eb0bee604d5d2061d85f.ci"  ""  "IN"  "CNAME"  "_9023f91de522527b4b669b841e4822fe.ltfvzjuylp.acm-validations.aws."))

(define guix.gnu.org-zone
  (knot-zone-configuration
    (domain "guix.gnu.org")
    (zone (zone-file
            (origin "guix.gnu.org")
            (ns primary-ns)
            (entries guix.gnu.org.zone)
            (serial 2023053014)))))
