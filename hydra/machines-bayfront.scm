(define harbourfront
  (build-machine
    (name "harbourfront.guix.info")
    (systems '("x86_64-linux" "i686-linux"))
    (user "bayfront")
    (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBfd3VZqyoKoZG3X772eDBrN2MPhnNDDnVA34hpf0f9b root@(none)")
    (parallel-builds 3)
    (speed 8.0)))
    
(define milano
  (build-machine
    (name "milano-guix-1.di.unimi.it")
    (systems '("x86_64-linux" "i686-linux"))
    (user "bayfront")
    (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPJFpBJSaOK2aDaR48Fm8udhLK9fl5YwNlq43uaLBeRv root@(none)")
    (parallel-builds 3)
    (speed 32.0)))

(define dover
  (build-machine
    (name "dover.guix.info")
    (port 9023)
    (systems '("aarch64-linux" "armhf-linux"))
    (user "bayfront")
    (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJLRYD5RXZ3Espe+Kv1SzZl8Qc3NZ356Bq+cGjnKsDHY root@linux")
    (parallel-builds 3)
    (speed 4.0)))

(define redhill
  (build-machine
    (name "redhill.guix.info")
    (port 9022)
    (systems '("armhf-linux"))
    (user "bayfront")
    (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM+FYr1NUTsXrJgquSTMz0Izkqw3ob7pCU+JPFHgEhHa root@redhill")
    (parallel-builds 3)
    (speed 4.0)))
 
(list
  harbourfront
  milano
  ; dover ; currently in the berlin build farm
  ; redhill ; currently not installed
)
