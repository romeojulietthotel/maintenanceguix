# DynamoDB

# DO NOT DELETE THIS TABLE!  It contains the Terraform locking state,
# shared by all Terraform users in the Guix project.  In addition, the
# s3 backend's locking feature will not function without it.
resource "aws_dynamodb_table" "terraform-locking" {
  name = "terraform-locking"
  # This table will not receive a steady, predictable rate of
  # requests.  In addition, the absolute number of requests will be
  # low.  Therefore, pay-per-request will be the most cost-effective.
  billing_mode = "PAY_PER_REQUEST"
  hash_key = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
  # Always encrypt data at rest.
  server_side_encryption {
    enabled = true
  }
}
