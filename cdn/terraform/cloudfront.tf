# CloudFront

locals {
  default_behavior = {
    # Only allow "read" verbs.
    allowed_methods = ["GET", "HEAD"]
    cached_methods = ["GET", "HEAD"]
    # The origin will compress data when necessary.
    compress = false
    # Cache responses that lack a Cache-Control header.
    default_ttl = 86400 # 1 day
    # When deciding whether or not to cache a response, ignore any
    # cookies, headers, or query strings that the client included in
    # their request.  This should increase the cache hit rate.  In
    # addition, this also causes CloudFront to omit these values
    # when forwarding the request to the custom origin. See:
    # https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/ConfiguringCaching.html
    forwarded_values = [{
      cookies = [{ forward = "none" }]
      query_string = false
    }]
    # Generally speaking, respect any Cache-Control or Expires
    # headers that the origin includes in its responses.  The
    # exception is that if a Cache-Control or Expires header says to
    # cache the result for more than 1 year, we ignore that and only
    # cache the result for 1 year at most.  Honestly, though, it
    # seems unrealistic to expect CloudFront to actually keep the
    # cached response for an entire year in that case.  See:
    # https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/Expiration.html
    max_ttl = 31536000 # 365 days
    min_ttl = 0
    target_origin_id = "ci.guix.info"
    # Historically, the build farm has allowed both HTTP and HTTPS.
    # We choose to maintain that policy here.  In the future, we
    # should consider changing this to "https-only".
    viewer_protocol_policy = "allow-all"
  }
  # Like the default behavior, but forward everything, and cache
  # nothing.  The CloudFront documentation says that to disable
  # caching, it is sufficient to forward all headers and set the
  # minimum TTL to 0, but we just forward everything and set all the
  # TTLs to 0 for good measure.  See:
  # https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/distribution-web-values-specify.html
  do_not_cache_behavior = {
    allowed_methods = ["GET", "HEAD"]
    # This list is not allowed to be empty.  See:
    # https://docs.aws.amazon.com/cloudfront/latest/APIReference/API_CachedMethods.html
    cached_methods = ["GET", "HEAD"]
    compress = false
    # Forward all cookies, headers, and query strings.
    forwarded_values = [{
      cookies = [{ forward = "all" }]
      headers = ["*"]
      query_string = true
    }]
    # Always serve the latest content from the origin.
    default_ttl = 0
    max_ttl = 0
    min_ttl = 0
    target_origin_id = "ci.guix.info"
    viewer_protocol_policy = "allow-all"
  }
}

resource "aws_cloudfront_distribution" "charlie-distribution" {
  enabled = false
  comment = "Distributed caching proxy for ci.guix.info"
  origin {
    domain_name = "ci.guix.info"
    origin_id = "ci.guix.info"
    custom_origin_config {
      http_port = 80 # Required, but not used.
      https_port = 443
      # Always use TLS when forwarding requests to the origin.
      origin_protocol_policy = "https-only"
      origin_ssl_protocols = ["TLSv1.2"]
      origin_keepalive_timeout = 60
      origin_read_timeout = 60
    }
  }
  # The CNAME that will point to this CloudFront distribution.
  aliases = ["ci.guix.gnu.org"]
  is_ipv6_enabled = true
  # This is actually the_maximum HTTP version to support. See:
  # https://www.terraform.io/docs/providers/aws/r/cloudfront_distribution.html#http_version
  http_version = "http2"
  # Serve requests from all edge locations.
  price_class = "PriceClass_All"
  # Do not restrict access.
  restrictions { geo_restriction { restriction_type = "none" }}
  # When deleting the distribution, actually delete it.  See:
  # https://www.terraform.io/docs/providers/aws/r/cloudfront_distribution.html#retain_on_delete
  retain_on_delete = false
  # By default, don't cache anything.  This is useful because on
  # ci.guix.info, we run Cuirass, which has many URLs we don't
  # want to cache.
  default_cache_behavior = ["${local.do_not_cache_behavior}"]
  # Cache all the relevant paths published by "guix publish".  See
  # guix/scripts/publish.scm in the Guix source for details.
  ordered_cache_behavior = [
    # /nix-cache-info
    "${merge(
      local.default_behavior,
      map("path_pattern", "/nix-cache-info")
    )}",
    # /<hash>.narinfo
    "${merge(
      local.default_behavior,
      map("path_pattern", "/*.narinfo")
    )}",
    # /file/<name>/sha256/<hash>
    "${merge(
      local.default_behavior,
      map("path_pattern", "/file/*")
    )}",
    # /log/<output>
    "${merge(
      local.default_behavior,
      map("path_pattern", "/log/*")
    )}",
    # /nar/gzip/<store-item>
    # /nar/<store-item>
    "${merge(
      local.default_behavior,
      map("path_pattern", "/nar/*")
    )}",
    # Static files for the Cuirass website - this is not part of "guix publish".
    "${merge(
      local.default_behavior,
      map("path_pattern", "/static/*")
    )}"
  ]
  # TODO: Maybe set a caching behavior for error responses.
  # custom_error_response {}
  viewer_certificate {
    # Note that "terraform apply" will fail until this certificate is
    # valid.  See the comment in the definition of
    # charlie-certificate for more information.
    acm_certificate_arn = "${aws_acm_certificate.charlie-certificate.arn}"
    # This is the recommended value as of 2018-12-28.  See:
    # https://docs.aws.amazon.com/cloudfront/latest/APIReference/API_ViewerCertificate.html
    # https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/secure-connections-supported-viewer-protocols-ciphers.html#secure-connections-supported-ciphers
    minimum_protocol_version = "TLSv1.1_2016"
    # Use SNI.  Don't use the "vip" (i.e., dedicated IP address)
    # method, since it's expensive and unnecessary.  See:
    # https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/cnames-https-dedicated-ip-or-sni.html
    ssl_support_method = "sni-only"
  }
}

output "charlie-distribution-id" {
  value = "${aws_cloudfront_distribution.charlie-distribution.id}"
}
output "charlie-distribution-enabled" {
  value = "${aws_cloudfront_distribution.charlie-distribution.enabled}"
}
output "charlie-distribution-status" {
  value = "${aws_cloudfront_distribution.charlie-distribution.status}"
}
output "charlie-distribution-domain-name" {
  value = "${aws_cloudfront_distribution.charlie-distribution.domain_name}"
}
