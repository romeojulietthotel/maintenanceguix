% The comment below tells Rubber to compile the .dot files.
%
% rubber: module graphics
% rubber: rules rules.ini

\documentclass[aspectratio=169]{beamer}

\usetheme{default}

\usefonttheme{structurebold}
\usepackage{helvet}
\usecolortheme{seagull}         % white on black

\usepackage[utf8]{inputenc}
\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref,xspace,multicol}
\usepackage[absolute,overlay]{textpos}
\usepackage{tikz}
\usetikzlibrary{arrows,shapes,trees,shadows,positioning}
\usepackage{fancyvrb}           % for \Verb

% For '⇝'.
\DeclareUnicodeCharacter{21DD}{$\rightsquigarrow$}

% Remember the position of every picture.
\tikzstyle{every picture}+=[remember picture]

\tikzset{onslide/.code args={<#1>#2}{%
  \only<#1>{\pgfkeysalso{#2}} % \pgfkeysalso doesn't change the path
}}

% Colors.
\definecolor{guixred1}{RGB}{226,0,38}  % red P
\definecolor{guixorange1}{RGB}{243,154,38}  % guixorange P
\definecolor{guixyellow}{RGB}{254,205,27}  % guixyellow P
\definecolor{guixred2}{RGB}{230,68,57}  % red S
\definecolor{guixred3}{RGB}{115,34,27}  % dark red
\definecolor{guixorange2}{RGB}{236,117,40}  % guixorange S
\definecolor{guixtaupe}{RGB}{134,113,127} % guixtaupe S
\definecolor{guixgrey}{RGB}{91,94,111} % guixgrey S
\definecolor{guixdarkgrey}{RGB}{46,47,55} % guixdarkgrey S
\definecolor{guixblue1}{RGB}{38,109,131} % guixblue S
\definecolor{guixblue2}{RGB}{10,50,80} % guixblue S
\definecolor{guixgreen1}{RGB}{133,146,66} % guixgreen S
\definecolor{guixgreen2}{RGB}{157,193,7} % guixgreen S

\setbeamerfont{title}{size=\huge}
\setbeamerfont{frametitle}{size=\huge}
\setbeamerfont{normal text}{size=\Large}

% White-on-black color theme.
\setbeamercolor{structure}{fg=guixorange1,bg=black}
\setbeamercolor{title}{fg=white,bg=black}
\setbeamercolor{date}{fg=guixorange1,bg=black}
\setbeamercolor{frametitle}{fg=white,bg=black}
\setbeamercolor{titlelike}{fg=white,bg=black}
\setbeamercolor{normal text}{fg=white,bg=black}
\setbeamercolor{alerted text}{fg=guixyellow,bg=black}
\setbeamercolor{section in toc}{fg=white,bg=black}
\setbeamercolor{section in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsubsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsubsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{frametitle in toc}{fg=white,bg=black}
\setbeamercolor{local structure}{fg=guixorange1,bg=black}

\newcommand{\highlight}[1]{\alert{\textbf{#1}}}

\title{Code Staging in GNU Guix}

\author{Ludovic Courtès}
\date{\small{GPCE\\23 October 2017, Vancouver, Canada}}

\setbeamertemplate{navigation symbols}{} % remove the navigation bar

\AtBeginSection[]{
  \begin{frame}
    \frametitle{}
    \tableofcontents[currentsection]
  \end{frame} 
}


\newcommand{\screenshot}[1]{
  \begin{frame}[plain]
    \begin{tikzpicture}[remember picture, overlay]
      \node [at=(current page.center), inner sep=0pt]
        {\includegraphics[width=\paperwidth]{#1}};
    \end{tikzpicture}
  \end{frame}
}


%% \usepackage{pgfpages}
%% \setbeameroption{second mode text on second screen}
%% \setbeameroption{previous slide on second screen}

\begin{document}

\begin{frame}[plain, fragile]
  \vspace{10mm}
  \titlepage

  \vfill{}
  \hfill{\includegraphics[width=0.2\paperwidth]{images/inria-logo-inverse-en-2017}}
\end{frame}

%% \begin{frame}[typeset second]
%%   This text is shown on the left and on the right.
%%   \only<second>{This text is only shown on the right.}
%%   \only<second:0>{This text is only shown on the left.}
%% \end{frame}

\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}
  \Huge{\textbf{Functional software deployment.}}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\setbeamercolor{normal text}{fg=white,bg=black}
\begin{frame}[fragile]

  \begin{semiverbatim}
    \Large{
\$ guix package -i emacs ocaml idris ghc
\textrm{...}

\$ guix package -r emacs -i vim
\textrm{...}
\pause
\$ guix package --roll-back
\textrm{...}
}
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]
  \begin{semiverbatim}
    \small{
(\alert{operating-system}
  (host-name "gastown")
  (timezone "Canada/Pacific")
  (locale "fr_CA.utf8")
  (bootloader (grub-configuration (device "/dev/sda")))
  (file-systems (cons (\alert{file-system}
                        (device "my-root")
                        (title 'label)
                        (mount-point "/")
                        (type "ext4"))
                      %base-file-systems))
  (users (cons (\alert{user-account}
                 (name "alice")
                 (group "users")
                 (home-directory "/home/alice"))
               %base-user-accounts))
  (services (cons (\alert{service} openssh-service-type)
                  %base-services)))
    }
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]

  \begin{semiverbatim}
    \Large{
\$ guix system vm config.scm
\textrm{...}

\$ guix system reconfigure config.scm
\textrm{...}

\$ guix system roll-back
\textrm{...}
}
  \end{semiverbatim}
\end{frame}

\setbeamercolor{normal text}{bg=guixdarkgrey,fg=guixred3}
\begin{frame}[fragile]
  \vspace{2cm}
  \Large{
    \textbf{Functional} software deployment paradigm:

    \begin{enumerate}
    \item build process = \highlight{pure function}
    \item built software = \highlight{persistent graph}
    \end{enumerate}
  }

  \vfill{}
  \small{
    \textit{Imposing a Memory Management Discipline on
      Software Deployment}, Dolstra et al., 2004 (Nix package manager)
  }
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\begin{frame}[fragile]
  \begin{semiverbatim}
    \small{
(define hello
  (\alert{package}
    (name "hello")
    (version "2.10")
    (source (\alert{origin}
              (method url-fetch)
              (uri (string-append "https://ftp.gnu.org/\textrm{...}/hello-"
                                  version ".tar.gz"))
              (sha256 (base32 "0wqd\textrm{...}dz6"))))
    (build-system gnu-build-system)
    (synopsis "An example GNU package")
    (description "Produce a friendly greeting.")
    (home-page "https://gnu.org/software/hello/")
    (license gpl3+)))
}
  \end{semiverbatim}

  \vfill{}
  \small{
    \textit{Functional Package Management with Guix}, Courtès, 2013
  }
\end{frame}

\begin{frame}[fragile]

  \begin{semiverbatim}
    \Large{
\$ guix build hello
\uncover<2->{/gnu/store/\tikz[baseline]{\node[anchor=base](nixhash){\alert<2>{sp4wxa09zxfcs4vchm6as014msa5yi22}};}-hello-2.10}
}
  \end{semiverbatim}

  \begin{textblock}{7}(4, 10)
    \only<2>{\tikz{\node(labelnixhash)[fill=white, text=black,
          rounded corners=2mm, inner sep=4mm]
        {\textbf{\Large{hash of \emph{all} the dependencies}};}}}
  \end{textblock}

  % Arrows
  \only<2>{
    \begin{tikzpicture}[overlay]
      \path[->](labelnixhash.north) edge [bend left, in=180, out=-45] (nixhash.south);
    \end{tikzpicture}
  }
\end{frame}

\setbeamercolor{normal text}{fg=black,bg=white}
\begin{frame}[fragile]{}
  \begin{tikzpicture}[tools/.style = {
                        text width=65mm, minimum height=3cm,
                        text badly ragged,
                        rounded corners=2mm,
                        fill=black, text=white
                      },
                      tool/.style = {
                        fill=black, text=white, text width=6cm,
                        text centered
                      },
                      daemon/.style = {
                        rectangle, text width=50mm, text centered,
                        rounded corners=2mm, minimum height=15mm,
                        top color=guixorange1,
                        bottom color=guixyellow,
                        text=black
                      },
                      builders/.style = {
                        draw=guixorange1, very thick, dashed,
                        fill=white, text=black, text width=5cm,
                        rounded corners=2mm,
                      },
                      builder/.style = {
                        draw=guixred2, thick, rectangle,
                        fill=guixgrey, rotate=90
                      }]
    \matrix[row sep=7mm, column sep=18mm] {
      \node(builders)[builders, text height=5cm]{}
          node[fill=white, text=black] at (0, 2) {\large{\textbf{build processes}}}
          node[fill=white, text=black] at (0, 1.5) {chroot, separate UIDs}
          node[builder, onslide=<1-2>{white}] at (-1,-0.5) {\alert<3->{Guile}, make, etc.}
          node[builder, onslide=<1-2>{white}] at ( 0,-0.5) {\alert<3->{Guile}, make, etc.}
          node[builder, onslide=<1-2>{white}] at ( 1,-0.5) {\alert<3->{Guile}, make, etc.}; &
      \node[tools]{}
          node[fill=black, text=white] at (0, 1) {\large{\textbf{Guile Scheme}}}
          node(client)[tool] at (0, 0)
          {\texttt{(define hello (\alert{package} \textrm{...}))}};
      \\

      \node(daemon)[daemon]{\large{\textbf{build daemon}}}; &
      &
      \\
    };
  \end{tikzpicture}

  \begin{tikzpicture}[overlay]
    \path[very thick, draw=guixorange1]<2->
      (client.south) edge [out=-90, in=0, ->, text=black] node[below, sloped]{RPCs} (daemon.east);
    \path[->, very thick, draw=guixorange1]<3->
      (daemon) edge (builders);
  \end{tikzpicture}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

% TODO: Scheme all the way down + image.

\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}
  \Huge{\textbf{Code staging.}}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\begin{frame}[fragile]{Staging: take \#1}
  \vspace{-0.2cm}
  \begin{semiverbatim}
(define \alert{build-exp}
  ;; \textsl{Build-side code.}
  '(symlink \only<1>{\textsl{"/gnu/store/123\textrm{...}-coreutils-8.25"}}\only<2->{(assoc-ref \alert{%build-inputs} "coreutils")}
            \only<1>{\textsl{"/gnu/store/abc\textrm{...}-result"}}\only<2->{\alert{%output}}))
\uncover<2->{
;; \textsl{... with unhygienic global variable:}
;; (define %build-inputs
;;   '(\only<2>{("coreutils" . "/gnu/store/\textrm{...}-coreutils-8.25")}\only<3>{}))

\uncover<2>{(define \alert{inputs}
  ;; \textsl{What goes into the chroot.}
  `(("coreutils" ,coreutils)))}

(build-expression->derivation store "symlink-to-coreutils"
                              \alert{build-exp}
                              \uncover<2>{#:inputs \alert{inputs}})}
\pause
  \end{semiverbatim}
\end{frame}

\setbeamercolor{normal text}{bg=guixdarkgrey}
\begin{frame}
  \Large{
    \begin{itemize}
    \item Nix: generates Bash code through string interpolation
    \item \textit{Writing Hygienic Macros in Scheme with Syntax-Case},
      (Dybvig, 1992)
    \item \textit{A Multi-Tier Semantics for Hop} (Serrano \& Queinnec,
      2010)
    \end{itemize}
  }
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\begin{frame}[fragile]{Take \#2: G-expressions}
  \begin{semiverbatim}
(define \alert{build-exp}
  ;; \textsl{First-class object that carries info}
  ;; \textsl{about its dependencies.}
  (\alert{gexp} (symlink (\alert{ungexp} coreutils)
                 (\alert{ungexp output}))))

;; \textsl{Leads to a build script like:}
;; (symlink "/gnu/store/123\textrm{...}-coreutils-8.25"
;;          (getenv "out"))

(gexp->derivation "symlink-to-coreutils" \alert{build-exp})
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]{Take \#2: G-expressions}
  \begin{semiverbatim}
(define \alert{build-exp}
  ;; \textsl{First-class object that carries info}
  ;; \textsl{about its dependencies.}
  \alert{#~}(symlink \alert{#$}coreutils \alert{#$output}))

;; \textsl{Leads to a build script like:}
;; (symlink "/gnu/store/\only<1>{123}\only<2>{h8a}\textrm{...}-coreutils-8.25"
;;          (getenv "out"))

(gexp->derivation "symlink-to-coreutils" \alert{build-exp}\only<1>{)}
                  \only<2>{\alert{#:system} "i686-linux")}
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]{Modules}
  \begin{semiverbatim}
(define \alert{build-exp}
\uncover<2>{  ;; \textsl{Compile (guix build utils) and add it}
  ;; \textsl{to the chroot.}
  (\alert{with-imported-modules} '((guix build utils))}
    \alert{#~}(begin
        (\alert{use-modules} (guix build utils))
        (mkdir-p (string-append \alert{#$output} "/bin"))\only<2>{)}

(gexp->derivation "empty-bin-dir" build-exp)
\only<1>{;; \textsl{ERROR: (guix build utils) not found!}}
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]{Initial RAM Disk}
  \begin{semiverbatim}
(expression->initrd
 (\alert{with-imported-modules} (source-module-closure
                         '((gnu build linux-boot)
                           (guix build utils)))
   \alert{#~}(begin
       (\alert{use-modules} (gnu build linux-boot)
                    (guix build utils))

       (boot-system #:mounts '#$file-systems
                    #:linux-modules '#$linux-modules
                    #:linux-module-directory '#$kodir)))
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]{Towards More Use Cases...}
  \begin{semiverbatim}
(\alert{eval-remotely} \alert{#~}(system* \alert{#$}(file-append ffmpeg "/bin/ffmpeg") \textrm{...})
               (open-ssh-session "example.org"))

(\alert{eval-in-vm} \alert{#~}(uname) vm)

\textrm{...}
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]{Scope Preservation}
  \vspace{-5mm}
  \begin{semiverbatim}
    \Large{
(let ((gen-body (lambda (x)
                  \alert{#~}(let ((x 40))
                      (+ x \alert{#$}x)))))
  \alert{#~}(let ((x 2))
      \alert{#$}(gen-body \alert{#~}x))
\pause
⇝ (let ((\alert{x-1bd8-0} 2))
    (let ((x-4f05-0 40))
      (+ x-4f05-0 \alert{x-1bd8-0})))
}
  \end{semiverbatim}
  \vspace{-10mm}
  \small{
    Dybvig 1992, Kiselyov 2008, Rhiger 2012
  }
\end{frame}

\setbeamercolor{normal text}{bg=guixred3}
\setbeamercolor{frametitle}{bg=guixred3}
\begin{frame}[fragile]{Limitations}
  \Large{
    \begin{itemize}
    \item hygiene vs. \highlight{macro-introduced bindings}?
    \item \highlight{modules} in scope?
    \item \highlight{serialization} of non-primitive data types?
    \item cross-stage \highlight{debugging info} à la Hop?
    \end{itemize}
  }
\end{frame}
\setbeamercolor{frametitle}{bg=black}
\setbeamercolor{normal text}{bg=black}

\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}
  \Huge{\textbf{Summary.}}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\setbeamercolor{normal text}{bg=guixgrey}
\begin{frame}[fragile]
  \LARGE{
    \begin{itemize}
    \item G-exps \highlight{tie staging to software deployment}
    \item \highlight{used at scale in Guix}
    \item entirely \highlight{implemented as a macro}
    \end{itemize}
  }
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[plain]

\vfill{
  \vspace{2.5cm}
  \center{\includegraphics[width=0.5\textwidth]{images/GuixSD-horizontal}}\\[1.0cm]
  \texttt{ludo@gnu.org}\hfill{\alert{\url{https://gnu.org/software/guix/}}}
}

\end{frame}

\setbeamercolor{normal text}{bg=guixred3}
\begin{frame}
  \Huge{\textbf{Ghost track!}}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\begin{frame}[fragile]{Cross-Compilation}
  \begin{semiverbatim}
(gexp->derivation "vi"
   \alert{#~}(begin
       (mkdir \alert{#$output})
       (system* (string-append \alert{#+}coreutils "/bin/ln")
                "-s"
                (string-append \alert{#$}emacs "/bin/emacs")
                (string-append \alert{#$}output "/bin/vi")))
\only<2>{   \alert{#:target} "mips64el-linux-gnu"})

;; \textsl{Yields:}
;; (begin
;;   (mkdir (getenv "out"))
;;   (system* (string-append "/gnu/store/123\textrm{...}" "/bin/ln")
;;            "-s"
;;            (string-append "/gnu/store/\only<1>{345}\only<2>{\alert{9ab}}\textrm{...}" \textrm{...})
;;            (string-append "/gnu/store/\only<1>{567}\only<2>{\alert{fc2}}\textrm{...}" \textrm{...})))
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]{Defining ``Compilers''}
  \begin{semiverbatim}
    \small{
(\alert{define-gexp-compiler} (package-compiler (package \alert{<package>})
                                        system target)
  ;; \textsl{Return a derivation to build PACKAGE.}
  (if target
      (package->cross-derivation package target system)
      (package->derivation package system)))
    }
\pause
(define-record-type \alert{<plain-file>}
  (plain-file name content)
  \textrm{...})

(\alert{define-gexp-compiler} (plain-file-compiler (file \alert{<plain-file>})
                                           system target)
  ;; \textsl{"Compile" FILE by adding it to the store.}
  (match file
    (($ \alert{<plain-file>} name content)
     (text-file name content))))
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]{Compilers \& ``Expanders''}
  \begin{semiverbatim}
\alert{#~}(string-append \alert{#$}coreutils "/bin/ls")

;; \textsl{Yields:}
;; (string-append "/gnu/store/\textrm{...}" "/bin/ls")
\pause
(file-append coreutils "/bin/ls")

;; \textsl{Yields:}
;; "/gnu/store/\textrm{...}/bin/ls"
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]
  \begin{overlayarea}{\textwidth}{8cm}
  \begin{tikzpicture}[kernel/.style = {
                        text width=10cm, minimum height=1.4cm,
                        text centered,
                        rounded corners=2mm,
                        fill=white, text=black
                      },
                      userland/.style = {
                        draw=guixorange1, very thick,
                        fill=white, text=black, text width=6cm,
                        rounded corners=2mm, minimum height=1.4cm,
                        text centered
                      }]
    \matrix[row sep=6mm, column sep=1cm] {
      \node(kernel)[kernel]{\textbf{\Large{Linux-libre}}};
      \\

      \node<2->(initrd)[userland]{\textbf{\Large{initial RAM disk}}};
      \\

      \node<4->(shepherd)[userland]{\textbf{\Large{PID 1: GNU Shepherd}}
        \\ services...};
      \\

      \node<6->(user)[userland, dashed]{\textbf{\Large{applications}}};
      \\
    };

    \path[->, very thick, draw=guixred1]<2->
      (kernel) edge (initrd);
    \path[->, very thick, draw=guixred1]<4->
      (initrd) edge (shepherd);
    \path[->, very thick, draw=guixred1]<6->
      (shepherd) edge (user);
    
  \end{tikzpicture}
  \end{overlayarea}

  \begin{tikzpicture}[overlay,
                      guile/.style = {
                         fill=guixyellow, text=black, rotate=30,
                         rounded corners=4mm, text width=3cm,
                         opacity=.75, text opacity=1, text centered,
                         minimum height=1.3cm
                      }]
    \node<3->(labelinitrd) [guile] at (initrd.east) {%
      \Large{Guile}
    };
    \node<5->(labelinitrd) [guile] at (shepherd.east) {%
      \Large{Guile}
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}{}

  \begin{textblock}{12}(2, 8)
    \tiny{
      Copyright \copyright{} 2010, 2012--2017 Ludovic Courtès \texttt{ludo@gnu.org}.\\[3.0mm]
      GNU GuixSD logo, CC-BY-SA 4.0, \url{https://gnu.org/s/guix/graphics}

      Copyright of other images included in this document is held by
      their respective owners.
      \\[3.0mm]
      This work is licensed under the \alert{Creative Commons
        Attribution-Share Alike 3.0} License.  To view a copy of this
      license, visit
      \url{http://creativecommons.org/licenses/by-sa/3.0/} or send a
      letter to Creative Commons, 171 Second Street, Suite 300, San
      Francisco, California, 94105, USA.
      \\[2.0mm]
      At your option, you may instead copy, distribute and/or modify
      this document under the terms of the \alert{GNU Free Documentation
        License, Version 1.3 or any later version} published by the Free
      Software Foundation; with no Invariant Sections, no Front-Cover
      Texts, and no Back-Cover Texts.  A copy of the license is
      available at \url{http://www.gnu.org/licenses/gfdl.html}.
      \\[2.0mm]
      % Give a link to the 'Transparent Copy', as per Section 3 of the GFDL.
      The source of this document is available from
      \url{http://git.sv.gnu.org/cgit/guix/maintenance.git}.
    }
  \end{textblock}
\end{frame}

\end{document}

% Local Variables:
% coding: utf-8
% comment-start: "%"
% comment-end: ""
% ispell-local-dictionary: "american"
% compile-command: "rubber --pdf talk.tex"
% End:
