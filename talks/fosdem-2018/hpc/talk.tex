% The comment below tells Rubber to compile the .dot files.
%
% rubber: module graphics
% rubber: rules rules.ini

\documentclass[aspectratio=169]{beamer}

\usetheme{default}

\usefonttheme{structurebold}
\usepackage{helvet}
\usecolortheme{seagull}         % white on black

\usepackage[utf8]{inputenc}
\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref,xspace,multicol}
\usepackage[absolute,overlay]{textpos}
\usepackage{tikz}
\usetikzlibrary{arrows,shapes,trees,shadows,positioning}
\usepackage{fancyvrb}           % for '\Verb'
\usepackage{xifthen}            % for '\isempty'

% Remember the position of every picture.
\tikzstyle{every picture}+=[remember picture]

\tikzset{onslide/.code args={<#1>#2}{%
  \only<#1>{\pgfkeysalso{#2}} % \pgfkeysalso doesn't change the path
}}

% Colors.
\definecolor{guixred1}{RGB}{226,0,38}  % red P
\definecolor{guixorange1}{RGB}{243,154,38}  % guixorange P
\definecolor{guixyellow}{RGB}{254,205,27}  % guixyellow P
\definecolor{guixred2}{RGB}{230,68,57}  % red S
\definecolor{guixred3}{RGB}{115,34,27}  % dark red
\definecolor{guixorange2}{RGB}{236,117,40}  % guixorange S
\definecolor{guixtaupe}{RGB}{134,113,127} % guixtaupe S
\definecolor{guixgrey}{RGB}{91,94,111} % guixgrey S
\definecolor{guixdarkgrey}{RGB}{46,47,55} % guixdarkgrey S
\definecolor{guixblue1}{RGB}{38,109,131} % guixblue S
\definecolor{guixblue2}{RGB}{10,50,80} % guixblue S
\definecolor{guixgreen1}{RGB}{133,146,66} % guixgreen S
\definecolor{guixgreen2}{RGB}{157,193,7} % guixgreen S

\setbeamerfont{title}{size=\huge}
\setbeamerfont{frametitle}{size=\huge}
\setbeamerfont{normal text}{size=\Large}

% White-on-black color theme.
\setbeamercolor{structure}{fg=guixorange1,bg=black}
\setbeamercolor{title}{fg=white,bg=black}
\setbeamercolor{date}{fg=guixorange1,bg=black}
\setbeamercolor{frametitle}{fg=white,bg=black}
\setbeamercolor{titlelike}{fg=white,bg=black}
\setbeamercolor{normal text}{fg=white,bg=black}
\setbeamercolor{alerted text}{fg=guixyellow,bg=black}
\setbeamercolor{section in toc}{fg=white,bg=black}
\setbeamercolor{section in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsubsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsubsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{frametitle in toc}{fg=white,bg=black}
\setbeamercolor{local structure}{fg=guixorange1,bg=black}

\newcommand{\highlight}[1]{\alert{\textbf{#1}}}

\title{Tying software deployment to scientific workflows}
\subtitle{Using GNU~Guix to make software deployment a first-class citizen}

\author{Ludovic Courtès}
\date{\small{FOSDEM 2018}}

\setbeamertemplate{navigation symbols}{} % remove the navigation bar

\AtBeginSection[]{
  \begin{frame}
    \frametitle{}
    \tableofcontents[currentsection]
  \end{frame} 
}


\newcommand{\screenshot}[2][width=\paperwidth]{
  \begin{frame}[plain]
    \begin{tikzpicture}[remember picture, overlay]
      \node [at=(current page.center), inner sep=0pt]
        {\includegraphics[{#1}]{#2}};
    \end{tikzpicture}
  \end{frame}
}


\begin{document}

\begin{frame}[plain, fragile]
  \vspace{10mm}
  \titlepage

  \vfill{}
  \hfill{\includegraphics[width=0.2\paperwidth]{images/inria-logo-inverse-en-2017}}
\end{frame}

\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}
  \Huge{\textbf{Guix in a nutshell.}}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\begin{frame}[fragile]

  \begin{semiverbatim}
    \Large{
\$ guix package \alert{--install} gcc-toolchain openmpi hwloc
\textrm{...}

\$ eval `guix package \alert{--search-paths}`
\textrm{...}

\$ guix package \alert{--manifest}=my-packages.scm
\textrm{...}

\$ guix package \alert{--roll-back}
\textrm{...}
}
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]
  \begin{semiverbatim}
    \Large{
bob@laptop$ guix \alert{pull} --commit=cabba9e
bob@laptop$ guix package \alert{-i} gcc-toolchain openblas


\pause


alice@supercomp$ guix \alert{pull} --commit=cabba9e
alice@supercomp$ guix package \alert{-i} gcc-toolchain openblas
}
  \end{semiverbatim}

  \begin{tikzpicture}[overlay]
    \node<3>[rounded corners=4, text centered,
          fill=guixorange1, text width=7cm,
          inner sep=3mm, opacity=.75, text opacity=1]
      at (current page.center) {
            \textbf{\Large{reproducible \& portable!}}
          };
  \end{tikzpicture}
\end{frame}

\begin{frame}[fragile]
  \begin{semiverbatim}
\Large{
\$ guix build hwloc \\
    \alert{--with-source}=./hwloc-2.0rc1.tar.gz
\textrm{...}

\pause
\$ guix package -i mumps \alert{--with-input}=scotch=pt-scotch
\textrm{...}

\pause
\$ guix package -i julia \alert{--with-input}=fftw=fftw-avx
\textrm{...}
}
  \end{semiverbatim}
\end{frame}


\begin{frame}
  \Large{
  \begin{itemize}
    \item started in 2012
    \item \highlight{6,800+ packages}, all free software
    \item x86\_64, i686, ARMv7, AArch64
    \item binaries at \url{https://hydra.gnu.org}
    \item 0.14.0 released in December 2017
  \end{itemize}
  }
\end{frame}

\setbeamercolor{normal text}{bg=white,fg=guixorange1}
\begin{frame}[fragile]
  \begin{tikzpicture}[overlay]
    \node(logo) [at=(current page.center), inner sep=0pt]
      {\includegraphics[width=\textwidth]{images/guixhpc-logo-transparent-white}};
    %% \node [at=(logo.south), anchor=north, text=black, inner sep=10pt]
    %%   {\Large{\textbf{Reproducible software deployment\\for high-performance computing.}}};
    \node [at=(current page.south), anchor=south, text=guixtaupe, inner sep=20pt]
      {\Large{\url{https://guix-hpc.bordeaux.inria.fr}}};
  \end{tikzpicture}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

% http://www.planetobserver.fr/wp-content/uploads/2016/03/Galerie_SAT15L8_Pacific_FrenchPolynesia_SocietyIslands_Tahiti_Moorea.jpg
% https://www.planetobserver.fr/wp-content/uploads/2016/03/Galerie_SAT15L8_Pacific_FrenchPolynesia_SocietyIslands_BoraBora.jpg
% http://upload.wikimedia.org/wikipedia/commons/b/b1/Galapagos-satellite-2002.jpg
%% \setbeamercolor{normal text}{bg=guixblue2}
%% \begin{frame}
%%   \Huge{\textbf{The archipelago of \\ ``tools that do one thing.''}}
%% \end{frame}
%% \setbeamercolor{normal text}{fg=white,bg=black}

\begin{frame}[fragile]
  \begin{tikzpicture}[overlay]
    \node [at=(current page.center), inner sep=0pt]
      {\includegraphics[width=1.2\textwidth]{images/Galapagos-satellite-2002}};
    \node [at=(current page.center), text=white, inner sep=20pt]
      {\LARGE{\textbf{The archipelago of ``tools that do one thing.''}}};
  \end{tikzpicture}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}


\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}
  \Huge{\textbf{Reproducible deployment\\ at the center of the stage.}}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\begin{frame}{``Package management''}
  \Large{
    \texttt{\$ guix \alert{package} -i openfoam emacs}
  }
\end{frame}

\begin{frame}[fragile]{``Virtual environments''}
  \Large{
    \begin{semiverbatim}
\$ git clone https://\textrm{...}/petsc
\$ cd petsc
\$ guix \alert{environment} petsc
[env]\$ ./configure && make    
    \end{semiverbatim}
  }
\end{frame}

\begin{frame}[fragile]{Container provisioning}
  \Large{
    \begin{semiverbatim}
\$ guix \alert{pack}\only<2->{ --format=docker} hwloc
\textrm{...}
/gnu/store/\textrm{...}-\only<1>{pack.tar.gz}\only<2>{docker-image.tar.gz}
    \end{semiverbatim}
  }
\end{frame}

\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}
  \Huge{\textbf{Intermezzo: the\\programming language underpinnings}}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\begin{frame}[fragile]{expression}
  \begin{semiverbatim}
    \LARGE{
(system* "/bin/lstopo")
      }
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]{staged expression}
  \begin{semiverbatim}
    \LARGE{
\highlight{'}(system* "/bin/lstopo")
      }
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]
  \frametitle{deployment-aware staged expression}

  \begin{semiverbatim}
    \LARGE{
\highlight{#~}(system* \highlight{#\$}(file-append \alert{hwloc} "/bin/lstopo"))
      }
  \end{semiverbatim}
\end{frame}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[fragile]
  \includegraphics[width=\textwidth]{images/hwloc-runtime-deps}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}[fragile]
  \Huge{\textbf{Guix Workflow Language}}
  \\
  {\large{\url{http://www.guixwl.org}}}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\setbeamercolor{normal text}{bg=white}
\begin{frame}
  \center{\includegraphics[height=0.9\textheight]{images/workflow-management-3}}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\begin{frame}[fragile]
  \begin{semiverbatim}
\alert{define-module}
    test
\alert{use-modules}
    guix workflows
    guix processes
    gnu packages bioinformatics
    gnu packages python

\alert{process}: simple-test
    \alert{package-inputs}
        list python samtools
    \alert{data-inputs}
        list "sample.bam" "hg38.fa" "abc"
    \alert{procedure} #---{python}
import os
print "hello from python 3"
---

\alert{workflow}: example-workflow
    \alert{processes}
        list simple-test
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]
  \begin{semiverbatim}
    \Large{
guix workflow --run=example \\
    -i input.dat -o output.dat \\
    --engine=\alert{grid-engine}
    }
  \end{semiverbatim}
\end{frame}

\setbeamercolor{normal text}{fg=black,bg=white}
% http://www.nature.com/ngeo/journal/v7/n11/full/ngeo2294.html
\screenshot{images/nature-transparency}

% https://www.nature.com/nmeth/journal/v12/n12/full/nmeth.3686.html
\screenshot{images/nature-reviewing-computational-methods}
% http://blogs.nature.com/methagora/2014/02/guidelines-for-algorithms-and-software-in-nature-methods.html

% http://www.acm.org/publications/policies/artifact-review-badging
%% \screenshot[height=\paperheight]{images/acm-artifact-review-and-badging}
\screenshot[width=.9\paperwidth]{images/acm-artifacts-evaluated-badge}

\screenshot{images/rescience}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \includegraphics[width=\textwidth]{images/big-picture-1}
\end{frame}
\begin{frame}[plain]
  \includegraphics[width=\textwidth]{images/big-picture-2}
\end{frame}
\begin{frame}[plain]
  \includegraphics[width=\textwidth]{images/big-picture-3}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

%% \begin{frame}
%%   \LARGE{Reproducible research demands\\
%%     reproducible software environments\\
%%     \& free software.}
%% \end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}[plain]
  \Huge{\textbf{Let's connect the bits!}}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[plain]

\vfill{
  \vspace{2.5cm}
  \center{\includegraphics[width=0.5\textwidth]{images/guixhpc-logo-transparent}}\\[1.0cm]
  \texttt{guix-hpc@gnu.org}
  \center{\alert{\url{https://hpc.guixsd.org/}}}
  \\[1cm]
}
\end{frame}

\begin{frame}{}

  \begin{textblock}{12}(2, 8)
    \tiny{
      Copyright \copyright{} 2010, 2012--2018 Ludovic Courtès \texttt{ludo@gnu.org}.\\[3.0mm]
      GNU Guix logo, CC-BY-SA 4.0, \url{http://gnu.org/s/guix/graphics}
      Workflow graph by Roel Janssen
      Galapagos satellite image, public domain (Earth Observatory 8270
      and NASA GSFC)
      Hand-drawn arrows by Freepik from flaticon.com

      Copyright of other images included in this document is held by
      their respective owners.
      \\[3.0mm]
      This work is licensed under the \alert{Creative Commons
        Attribution-Share Alike 3.0} License.  To view a copy of this
      license, visit
      \url{http://creativecommons.org/licenses/by-sa/3.0/} or send a
      letter to Creative Commons, 171 Second Street, Suite 300, San
      Francisco, California, 94105, USA.
      \\[2.0mm]
      At your option, you may instead copy, distribute and/or modify
      this document under the terms of the \alert{GNU Free Documentation
        License, Version 1.3 or any later version} published by the Free
      Software Foundation; with no Invariant Sections, no Front-Cover
      Texts, and no Back-Cover Texts.  A copy of the license is
      available at \url{http://www.gnu.org/licenses/gfdl.html}.
      \\[2.0mm]
      % Give a link to the 'Transparent Copy', as per Section 3 of the GFDL.
      The source of this document is available from
      \url{http://git.sv.gnu.org/cgit/guix/maintenance.git}.
    }
  \end{textblock}
\end{frame}

\end{document}

% Local Variables:
% coding: utf-8
% comment-start: "%"
% comment-end: ""
% ispell-local-dictionary: "american"
% compile-command: "rubber --pdf talk.tex"
% End:

%%  LocalWords:  Reproducibility
