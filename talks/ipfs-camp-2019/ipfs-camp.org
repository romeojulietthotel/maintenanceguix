#+AUTHOR: Pierre Neidhardt
#+EMAIL: mail@ambrevar.xyz
#+TITLE: IPFS & Guix
# #+TITLE: Package distribution using IPFS on Guix

#+OPTIONS: reveal_single_file:t

#+OPTIONS: toc:nil

#+REVEAL_ROOT: http://cdn.jsdelivr.net/reveal.js/3.0.0/
# #+REVEAL_TRANS: page
#+REVEAL_PLUGINS: (highlight)
#+REVEAL_THEME: night

#+REVEAL_MAX_SCALE: 0.6

#+REVEAL_TITLE_SLIDE_BACKGROUND: ./images/Guix_logo_noname.svg.png
#+REVEAL_TITLE_SLIDE_BACKGROUND_SIZE: 550px


#+BEGIN_NOTES
*IMPORTANT:* Read https://github.com/ipfs/roadmap
*IMPORTANT:* Read https://github.com/ipfs/package-managers#readme
#+END_NOTES

* IPFS
#+ATTR_REVEAL: :frag (appear)

Enhances accessibility to knowledge...

#+ATTR_REVEAL: :frag (appear)
...But stays blind to generated data.

* GNU Guix in short

Homepage: https://guix.gnu.org/

A functional package manager focusing on (among others):

- Reproducibility
  - https://reproducible-builds.org/
- Bootstrappability
  - GNU Mes: https://www.gnu.org/software/mes/
  - Stage0: https://github.com/oriansj/stage0

** Guix package substitutes

#+BEGIN_NOTES
Explain "Guix store".
#+END_NOTES

- A few build farms with limited throughput.
- Content delivery networks help (CDN).

Can we do better?

* IPFS & Guix: BFFs of the 21st century

# ** A common issue: augmenting trust in digital information
# - IPFS: Unalterable references to arbitrary data
# - Guix: Maximal transparency in software builds

- What IPFS can do for Guix:

  - Distribute Guix substitutes.
  - Unify references to source data.

- What Guix can provide for IPFS:

  - Maximal transparency in software builds.
  (Or any generated data?)

* Dreams of a (not so distant) future

** The dream: All Guix stores become substitute servers

P2P FTW!

** Deeper dream: All data lives in IPFS

No local filesystem, no Guix store.

Installing an application amounts to /pinning/ it.

#+BEGIN_NOTES
A personal computing device only stores references to information that its owner
cares about.

If we don't mind depending on IPFS, we don't need the Guix store any
more. Package installation would amount to local pinning. Anyone could then
build a package anywhere (home directory, ...) and just add it to IPFS.

(Local storage would be used as a cache for efficiency)
#+END_NOTES

** Abstract dream: All computations are equal

No distinction between "software builds" and everything else.


#+BEGIN_NOTES
Software builds are just a special case.
Examples: scientific computing, compression, generation...

Since that also eliminates the technical
constraints of the store, the same mechanism could be used for any kind
of data processing, with the results stored in IPFS. Reproducibility of
any kind of computation via Guix, with building software just an
important special case.
#+END_NOTES

#+BEGIN_NOTES
From https://github.com/ipfs/roadmap:

The rift between the web and the OS is finally healed. The OS and local programs
and WebApps merge. They are not just indistinguishable, they are the same
thing. "Installing" becomes pinning applications to the local computer. "Saving"
things locally is also just pinning. The browser and the OS are no longer
distinguishable. The entire OS data itself is modelled on top of IPLD, and the
internal FileSystem is IPFS (or something on top, like unixfs). The OS and
programs can manipulate IPLD data structures natively. The entire state of the
OS itself can be represented and stored as IPLD. The OS can be frozen or
snapshotted, and then resumed. A computer boots from the same OS hash,
drastically reducing attack surface.
#+END_NOTES

#+BEGIN_NOTES
Further discussion:

All data comes with provenance tracking:

- Computations are tracked via Guix.
- Human input is logged (interactivity) or version controlled.
#+END_NOTES

* Next step?

- IPFS wants YAPM (Yet Another Package Manager).

- IPFS wants WebOS.
