(use-modules (gnu)
             (gnu packages)
             (guix build-system font)
             (guix packages)
             (guix download)
             (guix utils)
             (guix licenses))

(define font-vollkorn
  (package
    (name "font-vollkorn")
    (version "4.105")
    (source (origin
              (method url-fetch/zipbomb)
              (uri (string-append
                    "http://vollkorn-typeface.com/download/vollkorn-"
                    (string-replace-substring version "." "-")
                    ".zip"))
              (sha256
               (base32
                "1xca3qq62ii6lh6j5lyg7kbvqa0pf8c12knkp3lz2akwjsfyll76"))))
    (build-system font-build-system)
    (home-page "http://vollkorn-typeface.com/")
    (synopsis "Serif typeface")
    (description "Vollkorn is a typeface for bread and butter use.")
    (license silofl1.1)))

(define font-league-gothic
  (package
    (name "font-league-gothic")
    (version "1.601")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://github.com/theleagueof/league-gothic/releases/"
                    "download/" version "/LeagueGothic-" version ".zip"))
              (sha256
               (base32
                "021smilmrqk70s4vgwry15zzah1cz3fkfmw5dkjvyvxsviz8xdxw"))))
    (build-system font-build-system)
    (home-page "https://www.theleagueofmoveabletype.com/league-gothic")
    (synopsis "Gothic typeface")
    (description "League Gothic is a revival of an old classic:
Alternate Gothic #1.  It was originally designed by Morris Fuller
Benton for the American Type Founders Company in 1903.  The company
went bankrupt in 1993, and since the original typeface was created
before 1923, the typeface is in the public domain.  League Gothic is
derived from this typeface.")
    (license silofl1.1)))

(packages->manifest
 (cons* font-vollkorn font-league-gothic
        (specifications->packages
         (list "font-fira-code" "ghostscript" "librsvg" "make"))))
