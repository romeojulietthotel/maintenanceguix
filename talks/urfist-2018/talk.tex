% The comment below tells Rubber to compile the .dot files.
%
% rubber: module graphics
% rubber: rules rules.ini

\documentclass[aspectratio=169]{beamer}

\usetheme{default}

\usefonttheme{structurebold}
\usepackage{helvet}
\usecolortheme{seagull}         % white on black

\usepackage[utf8]{inputenc}
\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref,xspace,multicol}
\usepackage[absolute,overlay]{textpos}
\usepackage{tikz}
\usetikzlibrary{arrows,shapes,trees,shadows,positioning}
\usepackage{fancyvrb}           % for '\Verb'
\usepackage{xifthen}            % for '\isempty'

% Remember the position of every picture.
\tikzstyle{every picture}+=[remember picture]

\tikzset{onslide/.code args={<#1>#2}{%
  \only<#1>{\pgfkeysalso{#2}} % \pgfkeysalso doesn't change the path
}}

% Colors.
\definecolor{guixred1}{RGB}{226,0,38}  % red P
\definecolor{guixorange1}{RGB}{243,154,38}  % guixorange P
\definecolor{guixyellow}{RGB}{254,205,27}  % guixyellow P
\definecolor{guixred2}{RGB}{230,68,57}  % red S
\definecolor{guixred3}{RGB}{115,34,27}  % dark red
\definecolor{guixorange2}{RGB}{236,117,40}  % guixorange S
\definecolor{guixtaupe}{RGB}{134,113,127} % guixtaupe S
\definecolor{guixgrey}{RGB}{91,94,111} % guixgrey S
\definecolor{guixdarkgrey}{RGB}{46,47,55} % guixdarkgrey S
\definecolor{guixblue1}{RGB}{38,109,131} % guixblue S
\definecolor{guixblue2}{RGB}{10,50,80} % guixblue S
\definecolor{guixgreen1}{RGB}{133,146,66} % guixgreen S
\definecolor{guixgreen2}{RGB}{157,193,7} % guixgreen S

\setbeamerfont{title}{size=\huge}
\setbeamerfont{frametitle}{size=\huge}
\setbeamerfont{normal text}{size=\Large}

% White-on-black color theme.
\setbeamercolor{structure}{fg=guixorange1,bg=black}
\setbeamercolor{title}{fg=white,bg=black}
\setbeamercolor{date}{fg=guixorange1,bg=black}
\setbeamercolor{frametitle}{fg=white,bg=black}
\setbeamercolor{titlelike}{fg=white,bg=black}
\setbeamercolor{normal text}{fg=white,bg=black}
\setbeamercolor{alerted text}{fg=guixyellow,bg=black}
\setbeamercolor{section in toc}{fg=white,bg=black}
\setbeamercolor{section in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsubsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsubsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{frametitle in toc}{fg=white,bg=black}
\setbeamercolor{local structure}{fg=guixorange1,bg=black}

\newcommand{\highlight}[1]{\alert{\textbf{#1}}}

\title{Au-delà des conteneurs : \\
  Environnements reproductibles \\
  avec GNU Guix}

\author{Ludovic Courtès}
\date{\small{URFIST de Bordeaux, 12 novembre 2018}}

\setbeamertemplate{navigation symbols}{} % remove the navigation bar

\AtBeginSection[]{
  \begin{frame}
    \frametitle{}
    \tableofcontents[currentsection]
  \end{frame} 
}


\newcommand{\screenshot}[2][width=\paperwidth]{
  \begin{frame}[plain]
    \begin{tikzpicture}[remember picture, overlay]
      \node [at=(current page.center), inner sep=0pt]
        {\includegraphics[{#1}]{#2}};
    \end{tikzpicture}
  \end{frame}
}


\begin{document}

\begin{frame}[plain, fragile]
  \vspace{10mm}
  \titlepage

  \vfill{}
  \hfill{\includegraphics[width=0.2\paperwidth]{images/inria-logo-inverse-en-2017}}
\end{frame}


\setbeamercolor{normal text}{fg=black,bg=white}
% http://www.nature.com/ngeo/journal/v7/n11/full/ngeo2294.html
%% \screenshot{images/nature-transparency}

% https://www.nature.com/nmeth/journal/v12/n12/full/nmeth.3686.html
%% \screenshot{images/nature-reviewing-computational-methods}
% http://blogs.nature.com/methagora/2014/02/guidelines-for-algorithms-and-software-in-nature-methods.html

% http://www.acm.org/publications/policies/artifact-review-badging
%% \screenshot[height=\paperheight]{images/acm-artifact-review-and-badging}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]{
      \includegraphics[height=.8\textheight]{images/acm-artifacts-functional}
    };
    \node [at=(current page.south), anchor=south,
      text=guixdarkgrey, fill=white, text opacity=1]{
      \small{\url{https://www.acm.org/publications/policies/artifact-review-badging}}
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]{
      \includegraphics[height=.8\textheight]{images/rescience}
    };
    \node [at=(current page.south), anchor=south,
      text=guixdarkgrey, fill=white, text opacity=1]{
      \small{\url{https://rescience.github.io/}}
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]{
      \includegraphics[width=.9\textwidth]{images/repeatability-study}
    };
    \node [at=(current page.south east), anchor=south east,
      text=guixdarkgrey, fill=white, text opacity=1]{
      \small{\url{http://reproducibility.cs.arizona.edu/}}
    };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \includegraphics[width=\textwidth]{images/big-picture-1}
\end{frame}
\begin{frame}[plain]
  \includegraphics[width=\textwidth]{images/big-picture-2}
\end{frame}
\begin{frame}[plain]
  \includegraphics[width=\textwidth]{images/big-picture-3}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
    {\includegraphics[width=\paperwidth]{images/IBM_Blue_Gene_P_supercomputer}};

    \node[at=(current page.center), rounded corners=4, text centered,
          inner sep=3mm, opacity=.75, text opacity=1]{
            \Huge{\textbf{HPC = cutting edge?}}
    };
  \end{tikzpicture}
\end{frame}

%% \setbeamercolor{normal text}{bg=guixblue2}
%% \begin{frame}
%%   \Huge{\textbf{Recipe for a contemporary HPC cluster environment.}}
%% \end{frame}
%% \setbeamercolor{normal text}{fg=white,bg=black}

\setbeamercolor{normal text}{fg=black,bg=white}
\screenshot{images/environment-modules}
\setbeamercolor{normal text}{fg=white,bg=black}

%% \setbeamercolor{normal text}{bg=white}
%% \screenshot[width=0.9\paperwidth]{images/package-managers-cropped}

%% \begin{frame}[plain]
%%   \begin{tikzpicture}[remember picture, overlay]
%%     \node [at=(current page.center), inner sep=0pt]
%%           {\includegraphics[height=\paperheight]{images/universal_install_script}};
%%     \node [at=(current page.north east), anchor=south east, rotate=90,
%%            text=black, text opacity=1, fill=white, opacity=.6]{
%%       \url{http://xkcd.com/1654/}
%%     };
%%   \end{tikzpicture}
%% \end{frame}

%% \setbeamercolor{normal text}{bg=guixblue2}
%% \begin{frame}
%%   \Huge{\textbf{Fixing HPC cluster environments.}}
%% \end{frame}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \begin{tikzpicture}[overlay]
    \node [at=(current page.center), anchor=south,
      fill=white, text width=\paperwidth, text centered,
      text height=0.5\paperheight]
          {\includegraphics[width=0.5\paperwidth]{images/easybuild}};

          % https://github.com/LLNL/spack/blob/develop/share/spack/logo/spack-logo-text-64.png
          % https://github.com/LLNL/spack/blob/develop/share/spack/logo/spack-logo-white-text-48.png
    \node [at=(current page.center), anchor=north,
      fill=white, text width=\paperwidth, text centered,
      inner sep=0.2\paperheight]
          {\includegraphics[width=0.4\paperwidth]{images/spack}};
  \end{tikzpicture}
\end{frame}

\screenshot[width=\paperwidth]{images/easybuild-bug}
%% \screenshot[width=\paperwidth]{images/spack-bug}

\setbeamercolor{normal text}{bg=guixdarkgrey}
\begin{frame}[plain]
  \Huge{\textbf{Approach \#2:\\``Preserve the mess''.}}
  \\[0.5cm]
  \hfill{\large{-- Arnaud Legrand (Inria reproducibility WG)}}
\end{frame}
\setbeamercolor{normal text}{bg=black}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[height=\paperheight]{images/docker-image-layers-cropped}};
    \node [at=(current page.north east), anchor=north east,
           text=black, text opacity=1, fill=white, opacity=.6]{
      \url{https://imagelayers.io/}
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=0.95\paperwidth]{images/singularity-hpc-wire}};
    \node [at=(current page.south east), anchor=south east,
           text=black, text opacity=1, fill=white]{
      \small{\url{https://www.hpcwire.com/2016/10/20/singularity-containers-easing-scientific-computing}}
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}[fragile]
  \begin{tikzpicture}[overlay]
    \node [at=(current page.center)]
      {\includegraphics[width=0.7\textwidth]{images/shrink-wrap}};
    \node<2> [at=(current page.east), anchor=east]
      {\includegraphics[width=0.4\textwidth]{images/shrink-wrap2}};
  \end{tikzpicture}
\end{frame}

% https://xkcd.com/1988/

%% \screenshot[width=\paperwidth]{images/rena-container-ship-wreck-nz}

\setbeamercolor{normal text}{bg=white,fg=guixorange1}
\begin{frame}[fragile]
  \begin{tikzpicture}[overlay]
    \node(logo) [at=(current page.center), inner sep=0pt]
      {\includegraphics[width=\textwidth]{images/guixhpc-logo-transparent-white}};
    %% \node [at=(logo.south), anchor=north, text=black, inner sep=10pt]
    %%   {\Large{\textbf{Reproducible software deployment\\for high-performance computing.}}};
    \node [at=(current page.south), anchor=south, text=guixdarkgrey, inner sep=20pt]
      {\Large{\url{https://guix-hpc.bordeaux.inria.fr}}};
  \end{tikzpicture}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% \begin{frame}
%%   \LARGE{
%%     \begin{enumerate}
%%     \item transactional package manager
%%     \item software environment manager
%%     \item APIs \& tools to customize environments
%%     \item container provisioning tools
%%     \end{enumerate}
%%   }
%% \end{frame}

\begin{frame}
  \Large{
  \begin{itemize}
    \item started in 2012
    \item \highlight{8,300+ packages}, all free software
    \item \highlight{4 architectures}:\\
      x86\_64, i686, ARMv7, AArch64
    \item binaries available
    \item \highlight{Guix-HPC effort (Inria, MDC, UBC) started in 2017}
  \end{itemize}
  }
\end{frame}

\begin{frame}{cluster deployments}
  \Large{
    \begin{itemize}
      % http://zvfak.blogspot.ch/2015/07/gnu-guix-for-easily-managing.html
    \item \highlight{Max Delbrück Center} (DE): 250-node cluster +
      workstations
      % https://ubc.uu.nl/infrastructure/
      % https://wiki.bioinformatics.umcutrecht.nl/pub/HPC/WebHome/HPC_Flyer.png
    \item \highlight{UMC Utrecht} (NL): 68-node cluster (1,000+ cores)
      % https://www.qriscloud.org.au/support/qriscloud-documentation/75-euramoo-datasheet
      % https://www.qriscloud.org.au/support/qriscloud-documentation/76-flashlite-datasheet
    \item \highlight{University of Queensland} (AU): 20-node cluster
      (900 cores)
    \item \highlight{PlaFRIM} (FR): Inria Bordeaux (3,000+ cores)
    \end{itemize}
  }
\end{frame}

\setbeamercolor{normal text}{bg=white}
%% \screenshot[width=.9\paperwidth]{images/openhub-activity}
\screenshot[width=.9\paperwidth]{images/openhub-contributors}
\setbeamercolor{normal text}{bg=black}


\begin{frame}[fragile]

  \begin{semiverbatim}
    \LARGE{
guix package \alert{-i} gcc-toolchain openmpi hwloc

eval `guix package \alert{--search-paths}=prefix`

guix package \alert{--roll-back}

guix package \alert{--profile}=./experiment \\
     -i gcc-toolchain@5.5 hwloc@1
}
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]
  \begin{semiverbatim}
    \LARGE{
guix package \alert{--manifest}=my-packages.scm



    (\alert{specifications->manifest}
      '("gcc-toolchain" "openmpi"
        "scotch" "mumps"))
}
  \end{semiverbatim}
\end{frame}

\setbeamercolor{normal text}{bg=guixdarkgrey}
\begin{frame}[fragile]
  \begin{semiverbatim}
    \Large{
bob@laptop$ guix package \alert{--manifest}=my-packages.scm
bob@laptop$ guix \alert{describe}
  guix cabba9e
    repository URL: https://git.sv.gnu.org/git/guix.git
    commit: cabba9e15900d20927c1f69c6c87d7d2a62040fe

\pause


alice@supercomp$ guix \alert{pull} --commit=cabba9e
alice@supercomp$ guix package \alert{--manifest}=my-packages.scm
}
  \end{semiverbatim}

  \begin{tikzpicture}[overlay]
    \node<3>[rounded corners=4, text centered, anchor=north,
          fill=guixorange1, text width=7cm,
          inner sep=3mm, opacity=.75, text opacity=1]
      at (current page.center) {
            \textbf{\Large{bit-reproducible \& portable!}}
          };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}[fragile]%{``Virtual environments''}
  \LARGE{
    \begin{semiverbatim}
\$ git clone https://\textrm{...}/petsc
\$ cd petsc
\$ guix \alert{environment} petsc
[env]\$ ./configure && make    
    \end{semiverbatim}
  }
\end{frame}

\begin{frame}[fragile]%{``Virtual environments''}
  \LARGE{
    \begin{semiverbatim}
\$ guix \alert{environment} --ad-hoc \\
      python python-numpy python-scipy \\
      -- python3
    \end{semiverbatim}
  }
\end{frame}

\setbeamercolor{normal text}{bg=guixred3}
\begin{frame}[fragile]%{Container provisioning}
  \LARGE{
    \begin{semiverbatim}
\$ guix \alert{pack}\only<2>{ --relocatable}\only<3>{ --format=squashfs}\only<4->{ --format=docker} \\
      jupyter jupyter-guile-kernel
\textrm{...}
/gnu/store/\textrm{...}-\only<1-2>{pack.tar.gz}\only<3>{singularity-image.tar.gz}\only<4->{docker-image.tar.gz}
    \end{semiverbatim}
  }
\end{frame}

\setbeamercolor{normal text}{bg=white}
\screenshot[width=.9\paperwidth]{images/docker-guix-lol}

\setbeamercolor{normal text}{bg=guixdarkgrey}
\begin{frame}[fragile]
  \begin{semiverbatim}
\LARGE{
guix pack hwloc \\
  \alert{--with-source}=./hwloc-2.1rc1.tar.gz


guix package -i mumps \\
  \alert{--with-input}=scotch=pt-scotch
}
  \end{semiverbatim}
\end{frame}

\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}[plain]
  \Huge{\textbf{Reproducible deployment is key.}}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\screenshot{images/pigx1}
\screenshot{images/pigx2}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]{
      \includegraphics[height=.7\textheight]{images/workflow-management-3}
    };
    \node [at=(current page.south), anchor=south,
      text=guixdarkgrey, fill=white, text opacity=1]{
      \small{\url{https://www.guixwl.org/}}
    };
    \node [at=(current page.north west), anchor=north west, opacity=0,
      inner sep=5mm, text=guixdarkgrey, fill=white, text opacity=1]{
      \Large{\textbf{Guix Workflow Language}}
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]{
      \includegraphics[width=.95\textwidth]{images/guix-kernel-python}
    };
    \node [at=(current page.south), anchor=south,
      text=guixdarkgrey, fill=white, text opacity=1]{
      \small{\url{https://gitlab.inria.fr/guix-hpc/guix-kernel/}}
    };
    \node [at=(current page.north west), anchor=north west, opacity=0,
      inner sep=5mm, text=guixdarkgrey, fill=white, text opacity=1]{
      \Large{\textbf{Jupyter + Guix (WIP!)}}
    };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}[plain]
  \Huge{\textbf{Wrap-up.}}
\end{frame}

\setbeamercolor{normal text}{bg=white}
\screenshot[width=\textwidth]{images/big-picture-3}

\setbeamercolor{normal text}{fg=white,bg=black}
\begin{frame}
  \LARGE{
    \begin{itemize}
    \item \highlight{reproduce} software environments
    \item \highlight{declare \& publish} complete environments
    \item beyond replication: precision \highlight{experimentation}
    \item a foundation for \highlight{``deployment-aware'' apps}
    \end{itemize}
  }
\end{frame}

\setbeamercolor{normal text}{bg=guixred3}
\begin{frame}[plain]
  \Huge{
    Scientists, developers,\\
    \& sysadmins:
    \alert{let's talk!}
  }
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setbeamercolor{normal text}{bg=black}
\begin{frame}[plain]

\vfill{
  \vspace{3cm}
  \center{\includegraphics[width=0.5\textwidth]{images/guixhpc-logo-transparent}}\\[1.0cm]
  \texttt{ludovic.courtes@inria.fr |} @GuixHPC
  \center{\alert{\url{https://guix-hpc.bordeaux.inria.fr}}}
  \\[1cm]
}
\end{frame}

\setbeamercolor{normal text}{bg=guixred2}
\begin{frame}
  \Huge{\textbf{Bonus slides!}}
\end{frame}

\setbeamercolor{normal text}{bg=black}
\begin{frame}[fragile]
  %% \frametitle{Bit-Reproducible Builds$^*$}
  %% \framesubtitle{$^*$ almost!}

  \begin{semiverbatim}
\Large{
\$ guix build hwloc
\uncover<2->{/gnu/store/\tikz[baseline]{\node[anchor=base](nixhash){\alert<2>{h2g4sf72\textrm{...}}};}-hwloc-1.11.2}

\uncover<3->{\$ \alert<3>{guix gc --references /gnu/store/\textrm{...}-hwloc-1.11.2}
/gnu/store/\textrm{...}-glibc-2.24
/gnu/store/\textrm{...}-gcc-4.9.3-lib
/gnu/store/\textrm{...}-hwloc-1.11.2
}}
  \end{semiverbatim}

  \begin{tikzpicture}[overlay]
    \node<1>(labelnixhash) [fill=white, text=black, inner sep=0.5cm,
       rounded corners] at (current page.center) {%
      \Large{\textbf{isolated build}: chroot, separate name spaces, etc.}
    };

    \node<2>(labelnixhash) [fill=white, text=black] at (4cm, 2cm) {%
      hash of \textbf{all} the dependencies};
    \path[->]<2>(labelnixhash.north) edge [bend left, in=180, out=-45] (nixhash.south);

    \draw<4-> (-10pt, 105pt) [very thick, color=guixorange2, rounded corners=8pt]
      arc (10:-50:-50pt and 110pt);
    \node<4->[fill=white, text=black, text opacity=1, opacity=.7,
          rounded corners=2mm, inner sep=5mm]
      at (7, 2) {\textbf{\Large{(nearly) bit-identical for everyone}}};
  \end{tikzpicture}

\end{frame}

\begin{frame}[fragile]
  \begin{tikzpicture}[overlay]
    % https://www.digitaldealer.com/wp-content/uploads/2014/01/transparency.jpg
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=\paperwidth]{images/transparency}};
    \node[at=(current page.center), anchor=east, rounded corners=4, text centered,
          inner sep=3mm, opacity=.75, text opacity=1, color=purple]{
            \Huge{\textbf{transparency?}}
    };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=0.95\paperwidth]{images/snap-crypto-miner}};
    \node [at=(current page.south east), anchor=south east,
           text=black, text opacity=1, fill=white]{
      \small{\url{https://github.com/canonical-websites/snapcraft.io/issues/651}}
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=0.9\paperwidth]{images/lwn-docker-hello-world}};
    \node [at=(current page.south east), anchor=south east,
           text=white, fill=black, text opacity=1]{
      \small{\url{https://lwn.net/Articles/752982/}}
    };
  \end{tikzpicture}
\end{frame}



\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[height=\paperheight]{images/hwloc-graph}};
  \end{tikzpicture}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}



\begin{frame}{}

  \begin{textblock}{12}(2, 8)
    \tiny{
      Copyright \copyright{} 2010, 2012--2018 Ludovic Courtès \texttt{ludo@gnu.org}.\\[3.0mm]
      GNU Guix logo, CC-BY-SA 4.0, \url{http://gnu.org/s/guix/graphics}
      Hand-drawn arrows by Freepik from flaticon.com

      Copyright of other images included in this document is held by
      their respective owners.
      \\[3.0mm]
      This work is licensed under the \alert{Creative Commons
        Attribution-Share Alike 3.0} License.  To view a copy of this
      license, visit
      \url{http://creativecommons.org/licenses/by-sa/3.0/} or send a
      letter to Creative Commons, 171 Second Street, Suite 300, San
      Francisco, California, 94105, USA.
      \\[2.0mm]
      At your option, you may instead copy, distribute and/or modify
      this document under the terms of the \alert{GNU Free Documentation
        License, Version 1.3 or any later version} published by the Free
      Software Foundation; with no Invariant Sections, no Front-Cover
      Texts, and no Back-Cover Texts.  A copy of the license is
      available at \url{http://www.gnu.org/licenses/gfdl.html}.
      \\[2.0mm]
      % Give a link to the 'Transparent Copy', as per Section 3 of the GFDL.
      The source of this document is available from
      \url{http://git.sv.gnu.org/cgit/guix/maintenance.git}.
    }
  \end{textblock}
\end{frame}

\end{document}

% Local Variables:
% coding: utf-8
% comment-start: "%"
% comment-end: ""
% ispell-local-dictionary: "francais"
% compile-command: "rubber --pdf talk.tex"
% End:

%%  LocalWords:  Reproducibility
