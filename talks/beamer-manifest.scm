;;; Released under the GNU GPLv3 or any later version.
;;; Copyright © 2021, 2023 Ludovic Courtès <ludo@gnu.org>

;; Manifest to create an environment to build LaTeX/Beamer slides.  Inspired
;; by <https://github.com/etu/presentations/blob/master/shell.nix#L4-L15>.

(specifications->manifest
 '("rubber"

   "texlive-base" "texlive-beamer"
   "texlive-fonts-ec" "texlive-etoolbox"
   "texlive-ulem"  "texlive-capt-of"
   "texlive-wrapfig" "texlive-latex-geometry"
   "texlive-latex-ms" "texlive-latex-graphics"
   "texlive-pgf" "texlive-translator"
   "texlive-latex-xkeyval" "texlive-mweights" "texlive-fontaxes"
   "texlive-latex-textpos" "texlive-latex-fancyvrb" "texlive-xifthen"
   "texlive-ifmtarg" "texlive-latex-upquote"

   ;; Additional fonts.
   "texlive-cm-super" "texlive-amsfonts"
   "texlive-fira" "texlive-inconsolata"))
