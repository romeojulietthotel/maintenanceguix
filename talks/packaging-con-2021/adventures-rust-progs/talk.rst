Packaging Rust crates in GNU Guix
=================================
|

.. figlet:: How   hard   could   it   possibly   be?

About me
========
|

    * Guix contributor since 2015
    * First Rust related commit December 2017
    * Primarily interested in distro maintenance

      * Watching packages compile is soothing
    * Part of my $DAYJOB is maintaining some Rust packages using Guix

Quick history of Rust and GNU Guix
==================================
|

    * First stable Rust release May 2015
    * First Rust related packaging bits in December 2016

      * (guix import crate foo)
      * 'Blessed Bootstrap Binaries' and 'rustc' package
      * 'cargo-build-system' added
    * 'cargo' added in April 2017
    * 'mrustc' added in April 2018

      * Use 'mrustc' for bootstrap path, September 2018
      * 'Blessed Bootstrap Binaries' removed
    * Add rust to icecat build, September 2018, Icecat-60.2.0
    * First crate added in April 2019
    * First 'rust app', rust-cbindgen, added October 2019, used for Icecat-68
    * First package to mix cargo-build-system with standard C code

      * librsvg-next, March 2020
      * Mixed rust code and C/C++ code, needs mixed build system
    * Install crates and source for later use, February 2021

      * $PREFIX/share/cargo{registry,src}

Looking at a standard package
=============================
|

.. code:: shell

    tree $(guix build sqlite)

|

.. code::

    /gnu/store/g9gf1ndxryjc15mrjiy41w162lx8j6cv-sqlite-3.32.3
    ├── bin
    │   └── sqlite3
    ├── include
    │   ├── sqlite3ext.h
    │   └── sqlite3.h
    ├── lib
    │   ├── libsqlite3.la
    │   ├── libsqlite3.so -> libsqlite3.so.0.8.6
    │   ├── libsqlite3.so.0 -> libsqlite3.so.0.8.6
    │   ├── libsqlite3.so.0.8.6
    │   └── pkgconfig
    │       └── sqlite3.pc
    └── share
        └── man
            └── man1
                └── sqlite3.1.gz
    /gnu/store/nhwgg7zbvqijwxi8djqqv5hmd940vqa1-sqlite-3.32.3-static
    └── lib
        └── libsqlite3.a

    8 directories, 10 files

Looking at a rust package - pre February 2021
=============================================

.. container:: handout

   This output needs to be refreshed
   pre commit 4d00185d66c9bd047dfe3077ed89a6a6129429ee

|

.. code:: shell

    tree $(guix build rust-rand@0.6)

|

.. code::

    /gnu/store/gx7lr6c45irpig0cy1wc36gasfh3yfic-rust-rand-0.6.5
    └── share
        └── doc
            └── rust-rand-0.6.5
                ├── LICENSE-APACHE
                └── LICENSE-MIT

Looking at a rust package now
=============================

.. container:: handout

   This output needs to be refreshed
   post commit 4d00185d66c9bd047dfe3077ed89a6a6129429ee

|

.. code:: shell

    tree $(guix build rust-rand@0.6)

.. code::

   /gnu/store/rvapywikakqwcby1qlz58h8rx8r6asv9-rust-rand-0.6.5
   └── share
       ├── cargo
       │   ├── registry
       │   │   └── rand-0.6.5.crate
       │   └── src
       │       └── rand-0.6.5
       │           ├── benches
       │           │   ├── distributions.rs
       │           │   ├── generators.rs
       │           │   ├── misc.rs
       │           │   └── seq.rs
       │           ├── build.rs
       ...
       └── doc
           └── rust-rand-0.6.5
               ├── LICENSE-APACHE
               └── LICENSE-MIT

|

A crate package definition:
===========================
|

.. code:: shell

    guix import crate rand@0.6

.. code:: scheme

  (define-public rust-rand-0.6
    (package
      (name "rust-rand")
      (version "0.6.5")
      (source ...)
      (build-system cargo-build-system)
      (arguments
       `(#:cargo-inputs
         (("rust-autocfg" ,rust-autocfg-0.1)
          ("rust-libc" ,rust-libc-0.2)
          ("rust-log" ,rust-log-0.4)
          ("rust-packed-simd" ,rust-packed-simd-0.3)
          ("rust-rand-chacha" ,rust-rand-chacha-0.1)
          ("rust-rand-core" ,rust-rand-core-0.4)
          ("rust-rand-hc" ,rust-rand-hc-0.1)
          ("rust-rand-isaac" ,rust-rand-isaac-0.1)
          ("rust-rand-jitter" ,rust-rand-jitter-0.1)
          ("rust-rand-os" ,rust-rand-os-0.1)
          ("rust-rand-pcg" ,rust-rand-pcg-0.1)
          ("rust-rand-xorshift" ,rust-rand-xorshift-0.1)
          ("rust-winapi" ,rust-winapi-0.3))
         #:cargo-development-inputs
         (("rust-average" ,rust-average-0.9)
          ("rust-rand-xoshiro" ,rust-rand-xoshiroi-0.1))))
      (home-page "https://crates.io/crates/rand")
      (synopsis "Random number generators and other randomness functionality.")
      (description "Random number generators and other randomness functionality.")
      (license (list license:expat license:asl2.0))))

A "normal" package definition:
==============================
|

.. code:: scheme

    (define-public newsboat
      (package
        (name "newsboat")
        (version "2.13")
        (source ...)
        (build-system gnu-build-system)
        (native-inputs
         `(("gettext" ,gettext-minimal)
           ("pkg-config" ,pkg-config)
           ("asciidoc" ,asciidoc))) ; For building documentation.
        (inputs
         `(("curl" ,curl)
           ("json-c" ,json-c)
           ("libxml2" ,libxml2)
           ("ncurses" ,ncurses)
           ("stfl" ,stfl)
           ("sqlite" ,sqlite)))
        (arguments
         '(#:phases
           (modify-phases %standard-phases
             (delete 'configure)            ; no configure script
             (add-after 'build 'build-documentation
               (lambda _ (invoke "make" "doc"))))
           #:make-flags (list (string-append "prefix=" (assoc-ref %outputs "out")))
           #:test-target "test"))
        (native-search-paths ; Newsboat respects CURL_CA_BUNDLE.
         (package-native-search-paths curl))
        (home-page "https://newsboat.org/")
        (synopsis "Text-mode RSS and Atom feed reader with podcast support")
        (description ...)
        (license (list license:gpl2+        ; filter/*
                       license:expat))))    ; everything else

Cargo inputs vs Inputs
======================
.. hidetitle::
.. figlet::
    CARGO   INPUTS   VS .   INPUTS

|

.. cowsay::
    what's the difference?

Output of $GUIX_ENVIRONMENT
===========================
|

.. code:: shell

   guix environment newsboat@2.13 -- sh -c 'ls $GUIX_ENVIRONMENT\bin'

|

.. code::

   '['           basename       bzless      clear         diff3       false        gcov         gzexe       ldd         lzfgrep    mktemp      msgunfmt           objdump         pwd               sh          sprof     tee        tzselect     who                                   xmlcatalog   yes
   a2x           basenc         bzmore      cmp           dir         fgrep        gcov-dump    gzip        ld.go       lzgrep     msgattrib   msguniq            od              ranlib            sha1sum     sqlite3   test       uname        whoami                                xmllint      zcat
   a2x.py        bash           c++         comm          dircolors   file         gcov-tool    head        link        lzless     msgcat      mtrace             paste           readelf           sha224sum   stat      tic        uncompress   x86_64-unknown-linux-gnu-c++          xtrace       zcmp
   addr2line     bashbug        captoinfo   cp            dirname     find         gencat       hostid      ln          lzma       msgcmp      mv                 patch           readlink          sha256sum   stdbuf    timeout    unexpand     x86_64-unknown-linux-gnu-g++          xz           zdiff
   ar            bunzip2        cat         cpp           du          fmt          getconf      iconv       locale      lzmadec    msgcomm     ncursesw6-config   pathchk         realpath          sha384sum   strings   toe        uniq         x86_64-unknown-linux-gnu-gcc          xzcat        zegrep
   as            bzcat          catchsegv   csplit        echo        fold         getent       id          localedef   lzmainfo   msgconv     ngettext           pcprofiledump   recode-sr-latin   sha512sum   strip     touch      unlink       x86_64-unknown-linux-gnu-gcc-7.5.0    xzcmp        zfgrep
   asciidoc      bzcmp          c++filt     curl          egrep       g++          gettext      infocmp     locate      lzmore     msgen       nice               pinky           reset             shred       stty      tput       unlzma       x86_64-unknown-linux-gnu-gcc-ar       xzdec        zforce
   asciidoc.py   bzdiff         chcon       curl-config   elfedit     gawk         gettextize   infotocap   logname     make       msgexec     nl                 pkg-config      rm                shuf        sum       tr         unxz         x86_64-unknown-linux-gnu-gcc-nm       xzdiff       zgrep
   autopoint     bzegrep        chgrp       cut           env         gawk-5.0.1   gettext.sh   install     ls          makedb     msgfilter   nm                 pldd            rmdir             size        sync      true       updatedb     x86_64-unknown-linux-gnu-gcc-ranlib   xzegrep      zless
   awk           bzfgrep        chmod       date          envsubst    gcc          gprof        join        lzcat       md5sum     msgfmt      nohup              pr              runcon            sleep       tabs      truncate   uptime       x86_64-unknown-linux-gnu-pkg-config   xzfgrep      zmore
   b2sum         bzgrep         chown       dd            expand      gcc-ar       grep         kill        lzcmp       mkdir      msggrep     nproc              printenv        sdiff             sort        tac       tset       users        xargs                                 xzgrep       znew
   base32        bzip2          chroot      df            expr        gcc-nm       groups       ld          lzdiff      mkfifo     msginit     numfmt             printf          sed               sotruss     tail      tsort      vdir         xgettext                              xzless
   base64        bzip2recover   cksum       diff          factor      gcc-ranlib   gunzip       ld.bfd      lzegrep     mknod      msgmerge    objcopy            ptx             seq               split       tar       tty        wc           xml2-config                           xzmore

Excerpt from cargo-build-system
===============================
|

.. code:: scheme

      ;; Configure cargo to actually use this new directory.
      (setenv "CARGO_HOME" (string-append (getcwd) "/.cargo"))
      (mkdir-p ".cargo")
      (let ((port (open-file ".cargo/config" "w" #:encoding "utf-8")))
        (display "
      [source.crates-io]
      replace-with = 'vendored-sources'

      [source.vendored-sources]
      directory = '" port)
        (display (string-append (getcwd) "/" vendor-dir) port)
        (display "'
      " port)
        (close-port port))

Air your Grievances
===================
|

.. cowsay::
   someone knows how to make it better

Results of FOSDEM 2020: Inputs
==============================
|

   * Guix

     * uses sources as extra inputs
     * ~2500 useless 'source only' packages
   * Debian

     * Mush together all the crate inputs
   * OpenSuSE

     * ... I should've taken notes
     * (call me!)

Results of FOSDEM 2020: Outputs
===============================
|

   * Guix

     * ~2500 useless 'source only' packages
     * untrackable dependency chains
   * Debian

     * Build all crates simultaneously
     * Many individual \*-dev source crates with feature flags
   * OpenSuSE

     * Makes the sources available to developers as bundles

Are they Packages or Sources?
=============================
|

.. cowsay::
   Cargo-inputs act as both

|

   * Added to deal with cyclical dependencies

     * We have \*-bootstrap packages to work around that
   * Hides the dependency chain

     * Unknown number of direct and transitive dependencies
     * Unable to fully test version changes

How can we fix this?
====================
|

   * Copy cargo-inputs to regular inputs

     * This tests to see what dependencies currently don't build
     * They get carried forward forever anyway
   * Start removing cargo-inputs

     * Either from specific packages or certain crates all at once
   * Move cargo-development-inputs to native-inputs

     * ... and hope this doesn't lead to circular dependencies

|

.. cowsay::
   Or you can disable tests since the development-inputs won't have their cargo inputs anyway

END SLIDE
=========
.. hidetitle::
.. figlet:: QUESTIONS?

| https://guix.gnu.org/
|
| #guix on irc.freenode.net
| guix-devel@gnu.org
| help-guix@gnu.org
|
| efraim@flashner.co.il
