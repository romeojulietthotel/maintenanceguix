#!/usr/bin/env bash

here=$(pwd)
cd src
if [ -f presentation.snm ]
then
    guix time-machine -C channels.scm \
         -- shell -C -m manifest.scm  \
         -- rubber --pdf support-notes-additionnelles.tex
fi
cd $here

if [ ! -f src/presentation.snm ]
then
    ./do-all.sh
fi
