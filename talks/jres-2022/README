# -*- mode:org -*-

Original Git repo: https://gitlab.com/zimoun/jres22-tuto-guix
SWH archive: https://archive.softwareheritage.org/swh:1:rev:8a378f2f833dd28ea71d38e7fc45bac10fe122d5

Visible: https://zimoun.gitlab.io/jres22-tuto-guix/
Web archive: https://web.archive.org/web/20220520150253/https://zimoun.gitlab.io/jres22-tuto-guix/

This repository contains the all material to run the tutorial about Guix
presented at [[https://www.jres.org/][JRES 2022]].  The language of the conference is French, thus this
material is initially written in French.

The version of the presentation for the D-day is tagged 'pres-d-day'
corresponnding to commit 6ff987583173ab5a0bc65708f8bf2482c7390eb0.

* Get PDF from source

#+begin_src bash
  cd src
  guix time-machine -C channels.scm \
       -- shell -C -m manifest.scm  \
       -- rubber --pdf presentation.tex
#+end_src

* Record Gif

#+begin_src bash
  export PS1="\[\033[0;34m\]alice@laptop:\w$ \[\033[0;30m\] "

  if [ -n "$GUIX_ENVIRONMENT" ]; then
      export PS1="\[\033[0;34m\]alice@laptop:\w\[\033[0;30m\] [env]$ "
  fi

#+end_src

#+begin_src bash
  # Fix <https://issues.guix.gnu.org/51202#3>
  # addition of: gtk+ gsettings-desktop-schemas
  guix time-machine -C channels.scm                                \
       -- shell peek ffmpeg         gtk+ gsettings-desktop-schemas \
       -- peek
#+end_src

* Capture Video

#+begin_src bash
  guix time-machine -C channels.scm   \
       -- shell obs                   \
       -- obs
#+end_src

** Resolution (screen from desktop)
  - 1920x1080 Base
  - 1920x1080 Output
  - Downscale filter: Bicubic (sharpened scaling, 16 samples)x
  - Common FPS values 60

** Recording / Streaming
  - 2500 Kbps / audio bitrate 160
  - Medium file size
  - mp4
  - x264

* Chapters

#+begin_src bash
  guix time-machine -C channels.scm   \
       -- shell ffmpeg python-minimal
#+end_src


Be careful, the Python help is a quick too simplistic script.

#+begin_src bash
  ffmpeg -i INPUT.mp4 -f ffmetadata META.txt
  python3 do-chapters.py CHAPS.txt
  cat META.txt CHAPS.txt.meta > CHAPS.meta
  ffmpeg -i INPUT.mp4 -i CHAPS.meta -map_metadata 1 -codec copy OUTPUT.mp4
#+end_src

For each video, end with a title at video length time.

* Merge videos

#+begin_src bash
  guix time-machine -C channels.scm   \
       -- shell ffmpeg python-minimal
#+end_src


The file =videos.txt= contains the list of videos

#+begin_src text
  file 'test.mp4'
  file 'test2.mp4'
#+end_src

#+begin_src bash
  ffmpeg -f concat -safe 0 -i videos.txt -c copy demos.mp4
#+end_src

#+begin_src bash
  ffmpeg -i INPUT.mp4 -f ffmetadata META.txt
  python3 do-chapters.py CHAPS.txt CHAPS2.txt
  cat META.txt merged.meta > video.meta
  ffmpeg -i video.mp4 -i video.meta -map_metadata 1 -codec copy video_chapters.mp4
#+end_src

The Python script =do-chapters.py= reads, at the finest, seconds when it would
require a finer scale.  Especially, see =08-multi-profiles-1.txt=.
